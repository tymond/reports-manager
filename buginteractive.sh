#!/bin/bash




NUMBER=5
TEMP_BUGS_LIST="tmp/buglists-temp.txt"
LOGFILE="changes.log"
SKIPPED_ISSUES_FILE="tmp/skippedBugs.txt"

echo $#
if [ $# -gt 0 ]
then
	NUMBER=$1
	echo "Number of entries changed to $NUMBER"
fi

./bugman.pl empty "${NUMBER}" | ./scripts/filterOutSkippedIssues.pl $SKIPPED_ISSUES_FILE | shuf | head -n $NUMBER > $TEMP_BUGS_LIST


echo "~~~ Initialization ~~~"
echo -n "Number of bugs that needs rating: "
./bugman.pl empty 1000 | wc -l
echo "Wishes list to check: "
cat $TEMP_BUGS_LIST
echo "~~~ Rating ~~~"




exec 3<"$TEMP_BUGS_LIST"
while IFS='' read -r -u 3 line || [[ -n "$line" ]]; do	

	echo ""
	echo "### wish: $line"
	BUGNO=`echo "$line" | cut -f1`
	HINT=`./wishman.pl gethint $BUGNO`
	if [[ $HINT != "" ]]
	then
		echo "Hint: $HINT"
	fi
	
	echo -n "Do you want to skip this bug report? (yes|no) (default: no): "
	read SKIP
	if [[ "$SKIP" == "" || "$SKIP" == "no" ]]; then
		#... do nothing
		echo -n ""
	else
		echo "Skipping..."
		echo "BugInteractive: $BUGNO SKIP" >> $LOGFILE
		# only explicit skipping puts the  issue into the skipped issues file, but that's intentional
		echo "$BUGNO" >> $SKIPPED_ISSUES_FILE
		continue;
	fi
	
	
	echo -n "Time needed (short medium long): "
	read TIME
	TIMEALLOWED=`echo -e "short\nmedium\nlong\n" | grep $TIME`
	if [ `echo -e "short\nmedium\nlong\n" | grep $TIME | wc -l` -ne 1 ]
	then
		echo "Wrong time! Skipping..."
		echo "BugInteractive: $BUGNO SKIP" >> $LOGFILE
		continue;
	else
		TIME=$TIMEALLOWED;
	fi
	
	
	echo -n "Perceived difficulty (easy medium difficult): "
	read DIFF
	DIFFALLOWED=`echo -e "easy\nmedium\ndifficult\n" | grep $DIFF`
	if [ `echo -e "easy\nmedium\ndifficult\n" | grep $DIFF | wc -l` -ne 1 ]
	then
		echo "Wrong difficulty! Skipping..."
		echo "BugInteractive: $BUGNO SKIP" >> $LOGFILE
		continue;
	else
		DIFF=$DIFFALLOWED;
	fi
	
	
	echo -n "Priority (verylow low medium high): "
	read PRIO
	PRIOALLOWED=`echo -e "verylow\nlow\nmedium\nhigh\n" | grep $PRIO`
	if [ "$PRIO" == "verylow" ] || [ "$PRIO" == "low" ] || [ "$PRIO" == "medium" ] || [ "$PRIO" == "high" ]
	then
		:
	else	
		if [ `echo -e "verylow\nlow\nmedium\nhigh\n" | grep $PRIO | wc -l` -ne 1 ]
		then
			echo "Wrong priority! Skipping..."
			echo "BugInteractive: $BUGNO SKIP" >> $LOGFILE
			continue;
		else
			PRIO=$PRIOALLOWED;
		fi
	fi
	
	
	echo -n "Does it need more (user-oriented) discussions? (ready unsure notready): "
	read READY
	READYALLOWED=`echo -e "ready\nunsure\nnotready\n" | grep $READY`
	echo "[$READYALLOWED]"
	if [ "$READY" == "ready" ] || [ "$READY" == "notready" ] || [ "$READY" == "unsure" ]
	then
		:
	else
		if [ `echo -e "ready\nunsure\nnotready\n" | grep $READY | wc -l` -ne 1 ]
		then
			echo "Wrong readiness! Skipping..."
			echo "BugInteractive: $BUGNO SKIP" >> $LOGFILE
			continue;
		else
			READY=$READYALLOWED;
		fi
	fi
	
	
	echo "Setting bug $BUGNO as [$PRIO] [$DIFF] [$TIME] [$READY]"
	echo "BugInteractive: $BUGNO SET [$PRIO] [$DIFF] [$TIME] [$READY]" >> $LOGFILE
	./bugman.pl put $BUGNO $PRIO $DIFF $TIME $READY &> /dev/null
    
done

./updateEverything.sh

echo "~~~ End ~~~"
echo "Thank you for contributing! <3"
echo "~~~ ~~~ ~~~"


