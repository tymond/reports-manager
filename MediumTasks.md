
## Tasks for developers wanting a bit more challenge

If you wish to implement one of the following wishes, please contact Krita team first!

### Wishes: 

#### * Unknown
- [ ] 475381	https://bugs.kde.org/show_bug.cgi?id=475381	Add a "Tab bars" option t othe Canvas-only settings

#### Animation
- [ ] 369503	https://bugs.kde.org/show_bug.cgi?id=369503	Render animation encode resume after error,by looking at folder
- [ ] 377731	https://bugs.kde.org/show_bug.cgi?id=377731	Layer colors filtering should show in timeline
- [ ] 393140	https://bugs.kde.org/show_bug.cgi?id=393140	Waveform in animation track
- [ ] 411109	https://bugs.kde.org/show_bug.cgi?id=411109	Add a way to render animation from prerendered image sequence
- [ ] 436964	https://bugs.kde.org/show_bug.cgi?id=436964	The video import dialog should have a cancel button
- [ ] 437021	https://bugs.kde.org/show_bug.cgi?id=437021	Set image type before Composition Export (Jpg, PNG, Alpha...)
- [ ] 437026	https://bugs.kde.org/show_bug.cgi?id=437026	Summary frames on groups
- [ ] 442754	https://bugs.kde.org/show_bug.cgi?id=442754	Animation timeline sets its min height to height of attached onion skin docker when using the corresponding button

#### Brush Engine/Bristle
- [ ] 395798	https://bugs.kde.org/show_bug.cgi?id=395798	Density in Bristle brush isn't pressure sensitive parameter and it isn't exposed in Popup Palette

#### Brush Engine/Shape
- [ ] 319446	https://bugs.kde.org/show_bug.cgi?id=319446	Use SVG brush file as shape library for animated brush. (Alchemy pull shapes feature)
- [ ] 410533	https://bugs.kde.org/show_bug.cgi?id=410533	SHIFT key doesn't change size
- [ ] 415932	https://bugs.kde.org/show_bug.cgi?id=415932	Some brushes doesn't follow their exact outlines as I draw with them
- [ ] 420095	https://bugs.kde.org/show_bug.cgi?id=420095	We should be easily able to choose another unit of mesurement instead of only "px", like "mm", "cm" and "in" at the tool options when drawing shapes

#### Brush engines
- [ ] 336731	https://bugs.kde.org/show_bug.cgi?id=336731	Saving color information in brush presets
- [ ] 340641	https://bugs.kde.org/show_bug.cgi?id=340641	Better accuracy for grid brush
- [ ] 344108	https://bugs.kde.org/show_bug.cgi?id=344108	Feathering option for Experiment brush
- [ ] 358183	https://bugs.kde.org/show_bug.cgi?id=358183	Feature request: support randomness/rotation in clone brush engine
- [ ] 367842	https://bugs.kde.org/show_bug.cgi?id=367842	Smudge Brush Radius could use a better sampling method.
- [ ] 375155	https://bugs.kde.org/show_bug.cgi?id=375155	Add option to texture each dab instead of stroke
- [ ] 376178	https://bugs.kde.org/show_bug.cgi?id=376178	EDIT "Brush Tip" button in brush settings.
- [ ] 382110	https://bugs.kde.org/show_bug.cgi?id=382110	Fill tool does not work if brush eraser is selected
- [ ] 383969	https://bugs.kde.org/show_bug.cgi?id=383969	Make brush color darker/lighter issue
- [ ] 388238	https://bugs.kde.org/show_bug.cgi?id=388238	Extension to the Particle Brush Engine - Add Color Source
- [ ] 390065	https://bugs.kde.org/show_bug.cgi?id=390065	Switch to erase mode when color picking from alpha (background)
- [ ] 391927	https://bugs.kde.org/show_bug.cgi?id=391927	Ability to export/import curves from the brush editor
- [ ] 392414	https://bugs.kde.org/show_bug.cgi?id=392414	Show on-canvas popup message when trying to paint on layers where that is not allowed
- [ ] 426790	https://bugs.kde.org/show_bug.cgi?id=426790	Request : Noise and Density features for both basic and custom brushes
- [ ] 436562	https://bugs.kde.org/show_bug.cgi?id=436562	MyPaint brushes ignores layer alpha locking

#### Color Selectors
- [ ] 363315	https://bugs.kde.org/show_bug.cgi?id=363315	Averaged Color Sampler
- [ ] 395600	https://bugs.kde.org/show_bug.cgi?id=395600	[Wishlist] Make it possible to choose different color selector styles in popup palette
- [ ] 397413	https://bugs.kde.org/show_bug.cgi?id=397413	Specific color selector misses HSV/HSL color space
- [ ] 407977	https://bugs.kde.org/show_bug.cgi?id=407977	Gamut mask status should be saved to Krita file
- [ ] 419208	https://bugs.kde.org/show_bug.cgi?id=419208	Add a way to save the palette that the Advanced Color Selector creates from an image
- [ ] 432847	https://bugs.kde.org/show_bug.cgi?id=432847	Option for pick colour "eyedropper" to select from all visible layers EXCEPT the current one

#### Color models
- [ ] 393477	https://bugs.kde.org/show_bug.cgi?id=393477	Add color mode type - RYB color wheel

#### Dockers
- [ ] 332040	https://bugs.kde.org/show_bug.cgi?id=332040	Selection Bag Docker.
- [ ] 395449	https://bugs.kde.org/show_bug.cgi?id=395449	Show used tool and/or brush in undo history entries
- [ ] 396197	https://bugs.kde.org/show_bug.cgi?id=396197	Touch docker(and another some dockers) needs horizontal layout
- [ ] 397432	https://bugs.kde.org/show_bug.cgi?id=397432	The overview docker does not show updates immediately
- [ ] 422721	https://bugs.kde.org/show_bug.cgi?id=422721	Merge artistic color wheel with gamut maps
- [ ] 447777	https://bugs.kde.org/show_bug.cgi?id=447777	make it so that the layers docker can be narrower

#### File formats
- [ ] 173590	https://bugs.kde.org/show_bug.cgi?id=173590	Provide visual feedback on loading files
- [ ] 382993	https://bugs.kde.org/show_bug.cgi?id=382993	32 bit EXR file opening time needs little improvement
- [ ] 400643	https://bugs.kde.org/show_bug.cgi?id=400643	PSD clipping layers not supported
- [ ] 420731	https://bugs.kde.org/show_bug.cgi?id=420731	:Feature Request: Support PAM files (P7)

#### Filter Layers
- [ ] 338002	https://bugs.kde.org/show_bug.cgi?id=338002	Arrange for layers
- [ ] 385845	https://bugs.kde.org/show_bug.cgi?id=385845	Add Film-like curve adjustment to keep constant hue during curves adjustment
- [ ] 387352	https://bugs.kde.org/show_bug.cgi?id=387352	Color Adjustment curves save and load option
- [ ] 420749	https://bugs.kde.org/show_bug.cgi?id=420749	The combination does not work: "Group / layer local selection"

#### Filters
- [ ] 341861	https://bugs.kde.org/show_bug.cgi?id=341861	"Low Frequency Even" like filter
- [ ] 352381	https://bugs.kde.org/show_bug.cgi?id=352381	A request for zoom and radial blur option in blur filter.
- [ ] 390822	https://bugs.kde.org/show_bug.cgi?id=390822	Color adjustment curves - no value picking from canvas
- [ ] 393221	https://bugs.kde.org/show_bug.cgi?id=393221	Request for better native Krita noise filter

#### General
- [ ] 135189	https://bugs.kde.org/show_bug.cgi?id=135189	Recombining different layers into one image (inverse of image separation)
- [ ] 333167	https://bugs.kde.org/show_bug.cgi?id=333167	GIMP 2.10 like high quality scaling algorithms
- [ ] 354318	https://bugs.kde.org/show_bug.cgi?id=354318	Add Other Measurement Unit Support for Grid/Shape Manipulation
- [ ] 364350	https://bugs.kde.org/show_bug.cgi?id=364350	Warn users if saving fails because of a lack of disk space
- [ ] 376942	https://bugs.kde.org/show_bug.cgi?id=376942	Grids don't take cm , mm, in as input, and are only pixel based
- [ ] 376943	https://bugs.kde.org/show_bug.cgi?id=376943	Guides should be editable with precision by allowing user to input the new value, and they shouldn't only use pixels as units
- [ ] 385728	https://bugs.kde.org/show_bug.cgi?id=385728	Autotrim to Image Size Option for Selection
- [ ] 401496	https://bugs.kde.org/show_bug.cgi?id=401496	Brush sensor curves: quick copy/paste buttons request
- [ ] 405604	https://bugs.kde.org/show_bug.cgi?id=405604	Choose Settings profile on launch
- [ ] 424643	https://bugs.kde.org/show_bug.cgi?id=424643	Can add to krita new export command on terminal for all Layer export? like export layer py plugin.
- [ ] 425554	https://bugs.kde.org/show_bug.cgi?id=425554	[Request] Trim layer(s) to selection
- [ ] 426087	https://bugs.kde.org/show_bug.cgi?id=426087	Automatically add wraparound image when using crop in wraparound mode
- [ ] 428934	https://bugs.kde.org/show_bug.cgi?id=428934	Show thumbnail when hovering over  composition (like layer popups)
- [ ] 447893	https://bugs.kde.org/show_bug.cgi?id=447893	Add the same option to export TIFF for the Comic Manager

#### Layer Stack
- [ ] 380141	https://bugs.kde.org/show_bug.cgi?id=380141	Make it possible to merge masks
- [ ] 410611	https://bugs.kde.org/show_bug.cgi?id=410611	Support drag + drop for external images onto a specific place in the Layer Stack

#### Layers/Vector
- [ ] 429567	https://bugs.kde.org/show_bug.cgi?id=429567	vector layers don't support filters

#### Scripting
- [ ] 390946	https://bugs.kde.org/show_bug.cgi?id=390946	Make metadata accessible through scripting

#### Shortcuts and Canvas Input Settings
- [ ] 368679	https://bugs.kde.org/show_bug.cgi?id=368679	Several frequently used commands are missing custom shortcuts
- [ ] 399803	https://bugs.kde.org/show_bug.cgi?id=399803	"New View" action missing in "Settings > Configure Krita... > Keyboard Shortcuts"
- [ ] 430692	https://bugs.kde.org/show_bug.cgi?id=430692	search for shortcuts by key

#### Tool/Text
- [ ] 391798	https://bugs.kde.org/show_bug.cgi?id=391798	Improve UX for Text Editor
- [ ] 423925	https://bugs.kde.org/show_bug.cgi?id=423925	Text Tool should remember last used settings

#### Tools
- [ ] 265801	https://bugs.kde.org/show_bug.cgi?id=265801	Add a "Create guide(s) from selected shape(s)" feature
- [ ] 324554	https://bugs.kde.org/show_bug.cgi?id=324554	export subtree
- [ ] 331344	https://bugs.kde.org/show_bug.cgi?id=331344	create infinite straight lines
- [ ] 333170	https://bugs.kde.org/show_bug.cgi?id=333170	Ability to save warp presets-FishEye and Inflate,etc.
- [ ] 334515	https://bugs.kde.org/show_bug.cgi?id=334515	Angle Constraints for PolygonalBrushTool and PolygonalSelectionTool
- [ ] 335880	https://bugs.kde.org/show_bug.cgi?id=335880	Hexagon Grid
- [ ] 344262	https://bugs.kde.org/show_bug.cgi?id=344262	Ruler missing percent measurement
- [ ] 352221	https://bugs.kde.org/show_bug.cgi?id=352221	Assignable Stabilization/Smoothing Presets
- [ ] 388006	https://bugs.kde.org/show_bug.cgi?id=388006	Tool for creating manga\comics grids
- [ ] 395231	https://bugs.kde.org/show_bug.cgi?id=395231	Pick color from "current and below layers" and "current and below layers without filters"
- [ ] 396798	https://bugs.kde.org/show_bug.cgi?id=396798	Rotate Selection Boxes with Canvas
- [ ] 397973	https://bugs.kde.org/show_bug.cgi?id=397973	Configurable angle snapping for line tool
- [ ] 407403	https://bugs.kde.org/show_bug.cgi?id=407403	Transformation and Mirror mode
- [ ] 409827	https://bugs.kde.org/show_bug.cgi?id=409827	Reference image Tool -> Color Tint
- [ ] 427860	https://bugs.kde.org/show_bug.cgi?id=427860	Show effect radius instead of brush outline as cursor for multibrush tool

#### Tools/Freehand
- [ ] 446787	https://bugs.kde.org/show_bug.cgi?id=446787	Implementing a new brush smoothing algorithm simply by up sampling and window averaging

#### Tools/Move
- [ ] 428177	https://bugs.kde.org/show_bug.cgi?id=428177	Moving object selections by keyboard

#### Tools/Reference Images
- [ ] 396155	https://bugs.kde.org/show_bug.cgi?id=396155	Reference images tool lacks features
- [ ] 405130	https://bugs.kde.org/show_bug.cgi?id=405130	Reference Images Tool: Color to Alpha and Invert Image built in

#### Tools/Transform
- [ ] 359128	https://bugs.kde.org/show_bug.cgi?id=359128	Add improved transformation filters (perspective transformation blurs the image)
- [ ] 371550	https://bugs.kde.org/show_bug.cgi?id=371550	Allow the transform tool to work on multiple selected layers

#### Tools/Vector
- [ ] 400521	https://bugs.kde.org/show_bug.cgi?id=400521	Performing a logical operation on vector objects adds many nodes

#### Usability
- [ ] 318027	https://bugs.kde.org/show_bug.cgi?id=318027	Apply inherit alpha layer to itself
- [ ] 335953	https://bugs.kde.org/show_bug.cgi?id=335953	[Brush-Presets-Docker]  Custom/Manual Sorting  ( feature request )
- [ ] 357493	https://bugs.kde.org/show_bug.cgi?id=357493	Remember color history after reopening a document
- [ ] 383587	https://bugs.kde.org/show_bug.cgi?id=383587	change axis of transform tool without changing the selection/image
- [ ] 386019	https://bugs.kde.org/show_bug.cgi?id=386019	Brushes displayed in brush editor may have a lower quality than strokes actually painted on the canvas
- [ ] 392225	https://bugs.kde.org/show_bug.cgi?id=392225	Preserve relative canvas position when switching from/into Canvas Only mode
- [ ] 397559	https://bugs.kde.org/show_bug.cgi?id=397559	Snapping is sloppy and unpredictable
- [ ] 410548	https://bugs.kde.org/show_bug.cgi?id=410548	Feedback in kis_delayed_save_dialog on the stroke that is blocking save.
- [ ] 415494	https://bugs.kde.org/show_bug.cgi?id=415494	[Feature request] Add a value to settings for changing discrete steps for canvas zoom, canvas rotate, brush size.
- [ ] 423200	https://bugs.kde.org/show_bug.cgi?id=423200	Showing both images in the Overwrite file dialog
- [ ] 433947	https://bugs.kde.org/show_bug.cgi?id=433947	Save several pressure curves
- [ ] 442287	https://bugs.kde.org/show_bug.cgi?id=442287	Replace fill layer's properties dialog color button with a color picker with instant preview

### Bugs: 

#### Animation
- [ ] 436060	https://bugs.kde.org/show_bug.cgi?id=436060	The horizontal Move shortcuts (cursor keys) do not work on animated frames
- [ ] 438771	https://bugs.kde.org/show_bug.cgi?id=438771	Animation curves - properties not always ordered

#### Brush engines
- [ ] 334452	https://bugs.kde.org/show_bug.cgi?id=334452	allow presets to override the cursor shown for certain modifiers such as ctrl.
- [ ] 380116	https://bugs.kde.org/show_bug.cgi?id=380116	Brushes with hsv settings and low opacity
- [ ] 408097	https://bugs.kde.org/show_bug.cgi?id=408097	Clone Tool leaves brush mark when trying to sample new area
- [ ] 413869	https://bugs.kde.org/show_bug.cgi?id=413869	Colour brush settings don't work when brush size is 1 pixel
- [ ] 419976	https://bugs.kde.org/show_bug.cgi?id=419976	c)_Pencil-3_Large_4B brush glitches on second picture
- [ ] 427104	https://bugs.kde.org/show_bug.cgi?id=427104	'Gradient Map' texturing mode interprets a pattern's alpha channel as the leftmost colour
- [ ] 433283	https://bugs.kde.org/show_bug.cgi?id=433283	[Clone engine] Source preview outline is shown on the current document despite the source being from another document
- [ ] 434373	https://bugs.kde.org/show_bug.cgi?id=434373	Certain mypaint brushes change dramatically from tweaking brush settings
- [ ] 434505	https://bugs.kde.org/show_bug.cgi?id=434505	Brush outline preview is translated away from real brush mark
- [ ] 438385	https://bugs.kde.org/show_bug.cgi?id=438385	Brush presets of Bristle and Filter engines are marked as modified by just switching to another preset

#### Color Selectors
- [ ] 372906	https://bugs.kde.org/show_bug.cgi?id=372906	Brush color changes after color picking with a noticeable delay
- [ ] 412333	https://bugs.kde.org/show_bug.cgi?id=412333	Cursor disappears after selecting color in another image.
- [ ] 422955	https://bugs.kde.org/show_bug.cgi?id=422955	Rotate brushcolor causes tonal value to change randomly

#### Color models
- [ ] 440073	https://bugs.kde.org/show_bug.cgi?id=440073	The K/L Darken/Lighten keys give very large changes for some colours

#### Dockers
- [ ] 411038	https://bugs.kde.org/show_bug.cgi?id=411038	Sessions do not work with multiple windows
- [ ] 419830	https://bugs.kde.org/show_bug.cgi?id=419830	Incorrect Thumbnail on both Group/Raster layer
- [ ] 430168	https://bugs.kde.org/show_bug.cgi?id=430168	Brush presets change when switching windows or selecting tools in the toolbox docker
- [ ] 446514	https://bugs.kde.org/show_bug.cgi?id=446514	Compositions docker not integrated with the undo stack

#### Documentation
- [ ] 447841	https://bugs.kde.org/show_bug.cgi?id=447841	Rate parameter in the Color section is not documented

#### Filter Layers
- [ ] 363153	https://bugs.kde.org/show_bug.cgi?id=363153	rounding errors when using burn on floating point images cause artefacts
- [ ] 432620	https://bugs.kde.org/show_bug.cgi?id=432620	Copy and pasting filter mask HSV/HSL Adjustment and Levels makes white pixels black
- [ ] 435942	https://bugs.kde.org/show_bug.cgi?id=435942	Layer groups do not properly animate transformations outside canvas

#### Filters
- [ ] 430372	https://bugs.kde.org/show_bug.cgi?id=430372	HSV Filter degrades quality of CMYK images
- [ ] 438418	https://bugs.kde.org/show_bug.cgi?id=438418	Oilpaint filter is super slow

#### General
- [ ] 390962	https://bugs.kde.org/show_bug.cgi?id=390962	Svg file with svg namespace prepended to each tag doesn't open right.
- [ ] 407896	https://bugs.kde.org/show_bug.cgi?id=407896	Memory indicator widget bar is showing the memory used amount wrong
- [ ] 409445	https://bugs.kde.org/show_bug.cgi?id=409445	No way to cancel autosave after trying to close the window

#### HDR
- [ ] 412533	https://bugs.kde.org/show_bug.cgi?id=412533	Small Color Selector resets the Hue when the nits slider changes
- [ ] 412534	https://bugs.kde.org/show_bug.cgi?id=412534	Changing nits slider doesn't update the color

#### Layers/Vector
- [ ] 405051	https://bugs.kde.org/show_bug.cgi?id=405051	Linear gradient from Inkscape not displayed correctly
- [ ] 408099	https://bugs.kde.org/show_bug.cgi?id=408099	SVG File layers are not rendered
- [ ] 411852	https://bugs.kde.org/show_bug.cgi?id=411852	'Reset Transformations' to reset size changes doesn't work on a single reference image.
- [ ] 421036	https://bugs.kde.org/show_bug.cgi?id=421036	Cannot load an svg file created by potrace
- [ ] 433220	https://bugs.kde.org/show_bug.cgi?id=433220	Krita doesn't export vector layers as SVG properly

#### Resize/Scale Image/Layer
- [ ] 429062	https://bugs.kde.org/show_bug.cgi?id=429062	Resizing image unproportionally deforms reference images too

#### Resource Management
- [ ] 440891	https://bugs.kde.org/show_bug.cgi?id=440891	Preset marked as dirty on startup

#### Scripting
- [ ] 421289	https://bugs.kde.org/show_bug.cgi?id=421289	Broken Channels management
- [ ] 426349	https://bugs.kde.org/show_bug.cgi?id=426349	Layers manipulation is buggy

#### Shortcuts and Canvas Input Settings
- [ ] 414243	https://bugs.kde.org/show_bug.cgi?id=414243	Personal Shortcuts Broken on New Version
- [ ] 437241	https://bugs.kde.org/show_bug.cgi?id=437241	If Canvas Input Settings are 'Paint Tool Sai Compatible', there is loss of functionality

#### Tablets (tablet issues are only very rarely bugs in Krita!)
- [ ] 423778	https://bugs.kde.org/show_bug.cgi?id=423778	Limited value range when painting with tangential pressure

#### Tool/Assistants
- [ ] 436422	https://bugs.kde.org/show_bug.cgi?id=436422	Concentric ellipse assistant tool fails to draw a perfect circle when using "snap to assistants"

#### Tool/Text
- [ ] 411739	https://bugs.kde.org/show_bug.cgi?id=411739	Inserting leading newlines creates text with broken state in text tool [SVG2, conversion to SVG]
- [ ] 428034	https://bugs.kde.org/show_bug.cgi?id=428034	"Align Left" in Krita keeps the text right-aligned for RTL languages
- [ ] 429515	https://bugs.kde.org/show_bug.cgi?id=429515	'Font Size' only displays integer numbers

#### Tools
- [ ] 396929	https://bugs.kde.org/show_bug.cgi?id=396929	Tools stop working after using Color Picker with "Sample Current Layer" on a hidden layer
- [ ] 427080	https://bugs.kde.org/show_bug.cgi?id=427080	Bug : Inconsistent functionality of the line tool
- [ ] 438889	https://bugs.kde.org/show_bug.cgi?id=438889	Can't cancel flood fill tool

#### Tools/Colorize
- [ ] 434266	https://bugs.kde.org/show_bug.cgi?id=434266	Visual artifacting when drawing after toggling "Limit to layer bounds" in Colorize Mask options

#### Tools/Freehand
- [ ] 351263	https://bugs.kde.org/show_bug.cgi?id=351263	Stabilizer causes jagged line endings without "stabilizer sensors" also enabled
- [ ] 414543	https://bugs.kde.org/show_bug.cgi?id=414543	Weighted brush smoothing doesn't work well.

#### Tools/Move
- [ ] 423587	https://bugs.kde.org/show_bug.cgi?id=423587	Snapping to grid doesn't work at all

#### Tools/Reference Images
- [ ] 435038	https://bugs.kde.org/show_bug.cgi?id=435038	Krita document DPI affects Reference Images: saved image is loaded differently on load.

#### Tools/Vector
- [ ] 408022	https://bugs.kde.org/show_bug.cgi?id=408022	Unstable vector gradient controller on lines with zero width bounding box
- [ ] 430774	https://bugs.kde.org/show_bug.cgi?id=430774	Gradient filling over Text objects can only makes them red.

#### Translation
- [ ] 410441	https://bugs.kde.org/show_bug.cgi?id=410441	Some categories in keyboard shortcuts are not translatable

#### Unittests
- [ ] 416592	https://bugs.kde.org/show_bug.cgi?id=416592	libs-ui-FreehandStrokeTest (Child aborted)
- [ ] 416593	https://bugs.kde.org/show_bug.cgi?id=416593	libs-ui-KisPaintOnTransparencyMaskTest (Child aborted)
- [ ] 416594	https://bugs.kde.org/show_bug.cgi?id=416594	libs-ui-FillProcessingVisitorTest (Failed)
- [ ] 416598	https://bugs.kde.org/show_bug.cgi?id=416598	libs-ui-KisDummiesFacadeTest (Child aborted)
- [ ] 416599	https://bugs.kde.org/show_bug.cgi?id=416599	libs-ui-KisZoomAndPanTest (Child aborted)
- [ ] 416600	https://bugs.kde.org/show_bug.cgi?id=416600	libs-ui-KisActionManagerTest (Child aborted)
- [ ] 416608	https://bugs.kde.org/show_bug.cgi?id=416608	plugins-impex-kis_csv_test (Child aborted)

#### Usability
- [ ] 375974	https://bugs.kde.org/show_bug.cgi?id=375974	canvas does not scroll up when moving cursor too slow
- [ ] 388394	https://bugs.kde.org/show_bug.cgi?id=388394	Modifier key Command -cmd- displayed as Control -ctrl-
- [ ] 392931	https://bugs.kde.org/show_bug.cgi?id=392931	Win-10 window position fault with 4.1.0 pre-alpha (g82a8db752) Window Layouts tool.
- [ ] 399347	https://bugs.kde.org/show_bug.cgi?id=399347	KoPopup Button sometimes pops popup widget up on the wrong monitor
- [ ] 403758	https://bugs.kde.org/show_bug.cgi?id=403758	Shift+Click-and-drag will move all the layers when using the "Move Tool" set to "Move current layer"
- [ ] 422045	https://bugs.kde.org/show_bug.cgi?id=422045	Advanced Colour Selector does not respond properly to HSV keyboard shortcuts shifts in some situations
- [ ] 424845	https://bugs.kde.org/show_bug.cgi?id=424845	Detaching the canvas annoyingly resizes the dockers to much larger sizes
- [ ] 425525	https://bugs.kde.org/show_bug.cgi?id=425525	Brush Settings options (size, ratio, etc.) don't remember which sensor you were looking at in certain cases
- [ ] 432882	https://bugs.kde.org/show_bug.cgi?id=432882	UI Font customization related issues
- [ ] 434228	https://bugs.kde.org/show_bug.cgi?id=434228	Can't invoke shortcuts which require holding a key while using non US layouts
- [ ] 439177	https://bugs.kde.org/show_bug.cgi?id=439177	Blending mode combobox dropdown keyboard behaviour is unintuitive

