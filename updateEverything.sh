#!/bin/bash


echo "update all"

./scripts/updateEasyTasksList.sh > /dev/null 2> /dev/null
./scripts/updateMediumTasksList.sh > /dev/null 2> /dev/null
./scripts/updateImportantBugsList.sh > /dev/null 2> /dev/null

echo "including small gsoc"
./scripts/updateSmallGSOCTasksList.sh #> /dev/null 2> /dev/null

./scripts/createChartsData.sh > /dev/null 2> /dev/null

