#!/bin/bash




NUMBER=5
TEMP_BUGS_LIST="tmp/wishlists-temp.txt"



echo $#
if [ $# -gt 0 ]
then
	NUMBER=$1
	echo "Number of entries changed to $NUMBER"
fi

./wishman.pl outdated | shuf | head -n $NUMBER > $TEMP_BUGS_LIST



echo "~~~ Initialization ~~~"
echo -n "Number of wishes that needs confirming: "
./wishman.pl outdated | wc -l

echo "Wishes list to check: "
cat $TEMP_BUGS_LIST
echo "NOTE: you can just write a part of what you want to do, as long as it doesn't repeat in other commands"
echo "For example 'c' will be treated as 'confirm'"
echo "~~~ Confirming ~~~"




exec 3<"$TEMP_BUGS_LIST"
while IFS='' read -r -u 3 line || [[ -n "$line" ]]; do	

	echo ""
	echo "### wish: $line"
	
	BUGNO=`echo "$line" | cut -f1`
	
	./wishman.pl gettoconfirm $BUGNO
	
	echo -n "Options: (rate skip confirm) "
	read OPTION
	OPTIONALLOWED=`echo -e "rate\nskip\nconfirm\n" | grep $OPTION`
	if [ `echo -e "rate\nskip\nconfirm\n" | grep $OPTION | wc -l` -ne 1 ]
	then
		echo "Wrong option! Skipping..."
		continue;
	else
		OPTION=$OPTIONALLOWED;
	fi
	
	if [[ "$OPTION" == "skip" ]]
	then
		continue;
	fi
	
	if [[ "$OPTION" == "confirm" ]] 
	then
		./wishman.pl confirm $BUGNO
		continue;
	fi
	
	
	echo -n "Time needed (short medium long): "
	read TIME
	TIMEALLOWED=`echo -e "short\nmedium\nlong\n" | grep $TIME`
	if [ `echo -e "short\nmedium\nlong\n" | grep $TIME | wc -l` -ne 1 ]
	then
		echo "Wrong time! Skipping..."
		continue;
	else
		TIME=$TIMEALLOWED;
	fi
	
	
	echo -n "Perceived difficulty (easy medium difficult): "
	read DIFF
	DIFFALLOWED=`echo -e "easy\nmedium\ndifficult\n" | grep $DIFF`
	if [ `echo -e "easy\nmedium\ndifficult\n" | grep $DIFF | wc -l` -ne 1 ]
	then
		echo "Wrong difficulty! Skipping..."
		continue;
	else
		DIFF=$DIFFALLOWED;
	fi
	
	
	echo -n "Priority (verylow low medium high): "
	read PRIO
	PRIOALLOWED=`echo -e "verylow\nlow\nmedium\nhigh\n" | grep $PRIO`
	if [ "$PRIO" == "verylow" ] || [ "$PRIO" == "low" ] || [ "$PRIO" == "medium" ] || [ "$PRIO" == "high" ]
	then
		:
	else	
		if [ `echo -e "verylow\nlow\nmedium\nhigh\n" | grep $PRIO | wc -l` -ne 1 ]
		then
			echo "Wrong priority! Skipping..."
			continue;
		else
			PRIO=$PRIOALLOWED;
		fi
	fi
	
	
	echo -n "Does it need more (user-oriented) discussions? (ready unsure notready): "
	read READY
	READYALLOWED=`echo -e "ready\nunsure\nnotready\n" | grep $READY`
	echo "[$READYALLOWED]"
	if [ "$READY" == "ready" ] || [ "$READY" == "notready" ] || [ "$READY" == "unsure" ]
	then
		:
	else
		if [ `echo -e "ready\nunsure\nnotready\n" | grep $READY | wc -l` -ne 1 ]
		then
			echo "Wrong readiness! Skipping..."
			continue;
		else
			READY=$READYALLOWED;
		fi
	fi
	
	
	echo "Setting bug $BUGNO as [$PRIO] [$DIFF] [$TIME] [$READY]"
	./wishman.pl put $BUGNO $PRIO $DIFF $TIME $READY &> /dev/null
    
done

./updateEverything.sh


echo "~~~ End ~~~"
echo "Thank you for contributing! <3"
echo "~~~ ~~~ ~~~"


