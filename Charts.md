## Chart 1: All

![Statistics in wishes chart](img/all_wishes.png "Statistics in wishes") ![Statistics in bugs chart](img/all_bugs.png "Statistics in bugs")

## Chart 2: Priorities

![Priorities in wishes chart](img/priorities_wishes.png "Priorities in wishes") ![Priorities in bugs chart](img/priorities_bugs.png "Priorities in bugs")

## Chart 2: Readiness

![Readiness in wishes chart](img/readiness_wishes.png "Readiness in wishes") ![Readiness in bugs chart](img/readiness_bugs.png "Readiness in bugs")
