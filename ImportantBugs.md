
## Bugs that are quite important to fix

If you wish to fix one of the following bugs, please contact Krita team first!

### Bugs: 

#### Animation
- [ ] 429326	https://bugs.kde.org/show_bug.cgi?id=429326	Recorder docker doesn't account for canvas size changes during drawing session.

#### Brush engines
- [ ] 344238	https://bugs.kde.org/show_bug.cgi?id=344238	Dynamic Brush Tool Doesn't Inherit Smoothing From Freehand Brush Tool Until Relaunch
- [ ] 419976	https://bugs.kde.org/show_bug.cgi?id=419976	c)_Pencil-3_Large_4B brush glitches on second picture
- [ ] 438385	https://bugs.kde.org/show_bug.cgi?id=438385	Brush presets of Bristle and Filter engines are marked as modified by just switching to another preset

#### Color models
- [ ] 434618	https://bugs.kde.org/show_bug.cgi?id=434618	color space not found

#### Dockers
- [ ] 419413	https://bugs.kde.org/show_bug.cgi?id=419413	Palette docker's swatch is lost forever if the palette's Column Count is set smaller than its position
- [ ] 429871	https://bugs.kde.org/show_bug.cgi?id=429871	Bad initial numbers when entering Pallete color RGB values
- [ ] 432184	https://bugs.kde.org/show_bug.cgi?id=432184	Properties buttons in Layers docker aren't greyed/hidden for layers that don't have properties dialogs

#### General
- [ ] 422688	https://bugs.kde.org/show_bug.cgi?id=422688	Scale Layer to New Size (Resize Layer) doesn't follow its exact number if I type those.

#### Resource Management
- [ ] 440891	https://bugs.kde.org/show_bug.cgi?id=440891	Preset marked as dirty on startup

#### Scripting
- [ ] 421289	https://bugs.kde.org/show_bug.cgi?id=421289	Broken Channels management
- [ ] 437068	https://bugs.kde.org/show_bug.cgi?id=437068	Python - Weird Zoom Bug

#### Shortcuts and Canvas Input Settings
- [ ] 414243	https://bugs.kde.org/show_bug.cgi?id=414243	Personal Shortcuts Broken on New Version
- [ ] 419587	https://bugs.kde.org/show_bug.cgi?id=419587	The Popup Palette Shortcut also affects canvas context menu

#### Tools
- [ ] 438889	https://bugs.kde.org/show_bug.cgi?id=438889	Can't cancel flood fill tool

#### Tools/Selection
- [ ] 424673	https://bugs.kde.org/show_bug.cgi?id=424673	While editing a selection, copy pasting the selection shape creates a new vector layer with the pasted object

#### Translation
- [ ] 410441	https://bugs.kde.org/show_bug.cgi?id=410441	Some categories in keyboard shortcuts are not translatable

#### Usability
- [ ] 422750	https://bugs.kde.org/show_bug.cgi?id=422750	On canvas HUD color picker (square type) or color history swatches are hard to use due to no buffer area on edges
- [ ] 424845	https://bugs.kde.org/show_bug.cgi?id=424845	Detaching the canvas annoyingly resizes the dockers to much larger sizes
- [ ] 428140	https://bugs.kde.org/show_bug.cgi?id=428140	Advanced Color Selector looks ugly in horizontal layout
- [ ] 434228	https://bugs.kde.org/show_bug.cgi?id=434228	Can't invoke shortcuts which require holding a key while using non US layouts

