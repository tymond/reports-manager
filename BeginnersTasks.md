
## Tasks for relative beginners

If you wish to implement one of the following wishes, please contact Krita team first!

### Wishes
#### Animation
- [ ] 376890	https://bugs.kde.org/show_bug.cgi?id=376890	onion skin for layer should be hidden if layer is not listed in the timeline
- [ ] 430489	https://bugs.kde.org/show_bug.cgi?id=430489	There is no lock button in animation-related dockers
- [ ] 466977	https://bugs.kde.org/show_bug.cgi?id=466977	Restore defaults option for the onion skin sliders

#### Color Selectors
- [ ] 423170	https://bugs.kde.org/show_bug.cgi?id=423170	JJ: MacOS' color picker pops up instead of Krita's internal color selector

#### Dockers
- [ ] 387187	https://bugs.kde.org/show_bug.cgi?id=387187	Brushes and stuff in dockers

#### File formats
- [ ] 423724	https://bugs.kde.org/show_bug.cgi?id=423724	Open both files when autosave present - Improvement to dialog

#### Filters
- [ ] 374726	https://bugs.kde.org/show_bug.cgi?id=374726	The levels filter does not have a channel selector

#### General
- [ ] 343654	https://bugs.kde.org/show_bug.cgi?id=343654	Forbid saving new workspace with name of an existent one + hide drop-down after selection.
- [ ] 388412	https://bugs.kde.org/show_bug.cgi?id=388412	[Wishlist/Feature Request] Cutter tool for Krita
- [ ] 396219	https://bugs.kde.org/show_bug.cgi?id=396219	Group Layer Export Wish : Autotrim to Visible Checkmark
- [ ] 420779	https://bugs.kde.org/show_bug.cgi?id=420779	Copy pasting issues with reference tool.
- [ ] 431201	https://bugs.kde.org/show_bug.cgi?id=431201	Auto-update of appimage: it should mark the new appimage as executable

#### HDR
- [ ] 406699	https://bugs.kde.org/show_bug.cgi?id=406699	No suitable cursor on HDR dark pictures
- [ ] 406743	https://bugs.kde.org/show_bug.cgi?id=406743	Default background color for HDR image is white

#### Layer Stack
- [ ] 373337	https://bugs.kde.org/show_bug.cgi?id=373337	Add blending mode to copy red channel to transparency
- [ ] 377034	https://bugs.kde.org/show_bug.cgi?id=377034	Save Group Layers Dimensions not Retained
- [ ] 400554	https://bugs.kde.org/show_bug.cgi?id=400554	Layer > Import/Export > Save Group Layers: export subgroups better
- [ ] 411823	https://bugs.kde.org/show_bug.cgi?id=411823	Properties window for Local Selection should be disabled
- [ ] 424070	https://bugs.kde.org/show_bug.cgi?id=424070	Convert layers to animation layer (and vice versa)
- [ ] 425951	https://bugs.kde.org/show_bug.cgi?id=425951	Feature request : New layer from selected layer stack
- [ ] 464051	https://bugs.kde.org/show_bug.cgi?id=464051	Focus to listLayers's center after layer change instead of just visible.

#### Layers/Vector
- [ ] 390950	https://bugs.kde.org/show_bug.cgi?id=390950	Support fractions for size and position of vector objects
- [ ] 392371	https://bugs.kde.org/show_bug.cgi?id=392371	Krita doesn't save "Keep aspect ratio" property of the shapes
- [ ] 396221	https://bugs.kde.org/show_bug.cgi?id=396221	Pasted vector layers always pasted at top, or in same vector layer
- [ ] 397212	https://bugs.kde.org/show_bug.cgi?id=397212	Add create vector to pull down layers in menu at top of Krita.
- [ ] 408777	https://bugs.kde.org/show_bug.cgi?id=408777	enable and show the color picker button in the fill option widget

#### Resize/Scale Image/Layer
- [ ] 432602	https://bugs.kde.org/show_bug.cgi?id=432602	crop / resize canvas to cm

#### Resource Management
- [ ] 367694	https://bugs.kde.org/show_bug.cgi?id=367694	Support loading palettes saved as a CSS file

#### Scripting
- [ ] 412846	https://bugs.kde.org/show_bug.cgi?id=412846	Expose ways to select/deselect nodes in the Layer docker
- [ ] 439650	https://bugs.kde.org/show_bug.cgi?id=439650	Add Canvas transformation method
- [ ] 477648	https://bugs.kde.org/show_bug.cgi?id=477648	Python API Active Tool request

#### Shortcuts and Canvas Input Settings
- [ ] 386202	https://bugs.kde.org/show_bug.cgi?id=386202	Mouse buttons as shortcuts
- [ ] 405677	https://bugs.kde.org/show_bug.cgi?id=405677	add a shortcut to toggle between selection tools (alternatively: between any specified tools)

#### Tagging
- [ ] 377495	https://bugs.kde.org/show_bug.cgi?id=377495	Tags: better feedback during bundle creation
- [ ] 428993	https://bugs.kde.org/show_bug.cgi?id=428993	Saving new brush presets misses tagging

#### Tool/Assistants
- [ ] 436213	https://bugs.kde.org/show_bug.cgi?id=436213	Shift key modifier on assistant tools should give 15 degree increments, not 90 degrees.

#### Tool/Text
- [ ] 392719	https://bugs.kde.org/show_bug.cgi?id=392719	Make it possible to move text objects when text tool is in use

#### Tools
- [ ] 333462	https://bugs.kde.org/show_bug.cgi?id=333462	A tool that crops canvas by pixel colour
- [ ] 417500	https://bugs.kde.org/show_bug.cgi?id=417500	Compositions Export options (png, jpg,etc...)
- [ ] 424331	https://bugs.kde.org/show_bug.cgi?id=424331	Add incrementing prefix option to Tools>Scripts>Export layers

#### Tools/Move
- [ ] 355336	https://bugs.kde.org/show_bug.cgi?id=355336	When move tool is selected use blending mode shortcuts to change layer blend modes

#### Tools/Reference Images
- [ ] 402610	https://bugs.kde.org/show_bug.cgi?id=402610	Add a subgrid to reference images
- [ ] 403111	https://bugs.kde.org/show_bug.cgi?id=403111	Reference picture from open pictures
- [ ] 437028	https://bugs.kde.org/show_bug.cgi?id=437028	Make it possibvle to copy- paste reference images

#### Tools/Selection
- [ ] 420666	https://bugs.kde.org/show_bug.cgi?id=420666	shortcut to duplicate the selected area
- [ ] 435876	https://bugs.kde.org/show_bug.cgi?id=435876	Shift modifier to crop a perfect square

#### Tools/Vector
- [ ] 417963	https://bugs.kde.org/show_bug.cgi?id=417963	The Freehand Path Tool needs a node type setting.
- [ ] 421524	https://bugs.kde.org/show_bug.cgi?id=421524	When you change the cap and join styles for a vector line, the next vectorline you draw won't have the same cap and join styles.

#### Usability
- [ ] 273901	https://bugs.kde.org/show_bug.cgi?id=273901	Add the option to change the softness (and other parameters of the brush) with floating menus
- [ ] 273903	https://bugs.kde.org/show_bug.cgi?id=273903	Add an in-canvas floating button to all transform tools to complete the operation
- [ ] 322159	https://bugs.kde.org/show_bug.cgi?id=322159	Add all node properties to the compositions docker
- [ ] 348922	https://bugs.kde.org/show_bug.cgi?id=348922	Wishbug : Extend the color patch ( previous and current color) while color picking to other tools such as gradient tools
- [ ] 349872	https://bugs.kde.org/show_bug.cgi?id=349872	Display names of missing resources in tooltip for presets docker with [x] icon
- [ ] 398935	https://bugs.kde.org/show_bug.cgi?id=398935	brush settings panel does not save menu state on re-open of krita
- [ ] 409184	https://bugs.kde.org/show_bug.cgi?id=409184	Feature Request: Ability to open image sequence and animated gif directly from file menu.
- [ ] 409253	https://bugs.kde.org/show_bug.cgi?id=409253	Feature request: new Rotate Canvas input mode
- [ ] 409946	https://bugs.kde.org/show_bug.cgi?id=409946	Allow users to define the grid spacing in the new document presets
- [ ] 411661	https://bugs.kde.org/show_bug.cgi?id=411661	"Select a Color" dialog is modeless when setting fg color and modal when setting bg color
- [ ] 411793	https://bugs.kde.org/show_bug.cgi?id=411793	Add an option in file menu to open the current file in file manager.
- [ ] 415600	https://bugs.kde.org/show_bug.cgi?id=415600	Wishlist: Improved and additional Image Resize functions
- [ ] 428866	https://bugs.kde.org/show_bug.cgi?id=428866	Better drawing of rulers
- [ ] 435293	https://bugs.kde.org/show_bug.cgi?id=435293	color history popup shouldn't appear centered
- [ ] 435300	https://bugs.kde.org/show_bug.cgi?id=435300	Show dockers should only show dockers
- [ ] 439362	https://bugs.kde.org/show_bug.cgi?id=439362	Consider always using native file dialog on Windows
- [ ] 443620	https://bugs.kde.org/show_bug.cgi?id=443620	Custom Pattern doesn't show Current Layer content until switched from and back

### Bugs
#### Animation
- [ ] 429326	https://bugs.kde.org/show_bug.cgi?id=429326	Recorder docker doesn't account for canvas size changes during drawing session.
- [ ] 437020	https://bugs.kde.org/show_bug.cgi?id=437020	Cancel ALL Composition Export

#### Brush Engine/Shape
- [ ] 410533	https://bugs.kde.org/show_bug.cgi?id=410533	SHIFT key doesn't change size

#### Brush engines
- [ ] 344238	https://bugs.kde.org/show_bug.cgi?id=344238	Dynamic Brush Tool Doesn't Inherit Smoothing From Freehand Brush Tool Until Relaunch
- [ ] 407901	https://bugs.kde.org/show_bug.cgi?id=407901	Standard filter blur  brush doesn't respond to Pressure/opacity slider

#### Color Selectors
- [ ] 441096	https://bugs.kde.org/show_bug.cgi?id=441096	Color selector doesn't intuitively update when selecting a different color model type in Color Selector Settings

#### Color models
- [ ] 434618	https://bugs.kde.org/show_bug.cgi?id=434618	color space not found

#### Dockers
- [ ] 419413	https://bugs.kde.org/show_bug.cgi?id=419413	Palette docker's swatch is lost forever if the palette's Column Count is set smaller than its position
- [ ] 429871	https://bugs.kde.org/show_bug.cgi?id=429871	Bad initial numbers when entering Pallete color RGB values
- [ ] 432184	https://bugs.kde.org/show_bug.cgi?id=432184	Properties buttons in Layers docker aren't greyed/hidden for layers that don't have properties dialogs

#### General
- [ ] 363971	https://bugs.kde.org/show_bug.cgi?id=363971	Autosave file for the original document not deleted when saving as another file
- [ ] 410429	https://bugs.kde.org/show_bug.cgi?id=410429	Cancel button works like OK button for Python plugin settings
- [ ] 419467	https://bugs.kde.org/show_bug.cgi?id=419467	Split layer can take hours on images with many colors
- [ ] 422688	https://bugs.kde.org/show_bug.cgi?id=422688	Scale Layer to New Size (Resize Layer) doesn't follow its exact number if I type those.
- [ ] 431755	https://bugs.kde.org/show_bug.cgi?id=431755	Krita continues running in the background after closing session manager

#### HDR
- [ ] 433461	https://bugs.kde.org/show_bug.cgi?id=433461	Cannot change exposure for HDR image

#### Layers/Vector
- [ ] 403676	https://bugs.kde.org/show_bug.cgi?id=403676	Converting vector layer to paint layer crops the content of the vector layer beyond canvas boundaries

#### Scripting
- [ ] 437068	https://bugs.kde.org/show_bug.cgi?id=437068	Python - Weird Zoom Bug

#### Shortcuts and Canvas Input Settings
- [ ] 419587	https://bugs.kde.org/show_bug.cgi?id=419587	The Popup Palette Shortcut also affects canvas context menu

#### Tool/Assistants
- [ ] 428317	https://bugs.kde.org/show_bug.cgi?id=428317	Tracking rulers update at <1 FPS when non brush tool selected.

#### Tool/Text
- [ ] 436872	https://bugs.kde.org/show_bug.cgi?id=436872	Kerning toggle

#### Tools
- [ ] 430957	https://bugs.kde.org/show_bug.cgi?id=430957	Fill tool too picky with the colour of full transparent pixels.

#### Tools/Colorize
- [ ] 439889	https://bugs.kde.org/show_bug.cgi?id=439889	The bottom progress bar is inconsistent with the progress bar on the Colorize Mask layer.

#### Tools/Selection
- [ ] 424673	https://bugs.kde.org/show_bug.cgi?id=424673	While editing a selection, copy pasting the selection shape creates a new vector layer with the pasted object

#### Translation
- [ ] 422380	https://bugs.kde.org/show_bug.cgi?id=422380	Keyboard Shortcut's action 1st category names cannot be translated

#### Unittests
- [ ] 416601	https://bugs.kde.org/show_bug.cgi?id=416601	libs-ui-KisCategoriesMapperTest (Failed)

#### Usability
- [ ] 408440	https://bugs.kde.org/show_bug.cgi?id=408440	Landing page defaults eraser brush to 'off'
- [ ] 414259	https://bugs.kde.org/show_bug.cgi?id=414259	Inconsistent scaling with vertically tall, thin predefined brush tips
- [ ] 422750	https://bugs.kde.org/show_bug.cgi?id=422750	On canvas HUD color picker (square type) or color history swatches are hard to use due to no buffer area on edges
- [ ] 428140	https://bugs.kde.org/show_bug.cgi?id=428140	Advanced Color Selector looks ugly in horizontal layout
- [ ] 431164	https://bugs.kde.org/show_bug.cgi?id=431164	Custom Document inherits settings utilized in Create from Clipboard as new defaults
- [ ] 438435	https://bugs.kde.org/show_bug.cgi?id=438435	More than two out of the four available expanding spacers across two toolbars cannot be displayed properly

