
# Tasks for small GSOC

This list is mostly for internal use: as an auto-generated list of ideas that can be used for ideas for GSOC students in 2021 and future years, since GSOC now is smaller than in previous years

If you wish to implement one of the following wishes, please contact Krita team first!

## High priority, easy:

#### Animation
- [ ] 437026	https://bugs.kde.org/show_bug.cgi?id=437026	Summary frames on groups

#### Brush engines
- [ ] 391927	https://bugs.kde.org/show_bug.cgi?id=391927	Ability to export/import curves from the brush editor

#### Color Selectors
- [ ] 363315	https://bugs.kde.org/show_bug.cgi?id=363315	Averaged Color Sampler
- [ ] 395600	https://bugs.kde.org/show_bug.cgi?id=395600	[Wishlist] Make it possible to choose different color selector styles in popup palette

#### Dockers
- [ ] 396197	https://bugs.kde.org/show_bug.cgi?id=396197	Touch docker(and another some dockers) needs horizontal layout
- [ ] 422721	https://bugs.kde.org/show_bug.cgi?id=422721	Merge artistic color wheel with gamut maps

#### File formats
- [ ] 173590	https://bugs.kde.org/show_bug.cgi?id=173590	Provide visual feedback on loading files

#### Filter Layers
- [ ] 387352	https://bugs.kde.org/show_bug.cgi?id=387352	Color Adjustment curves save and load option

#### General
- [ ] 376943	https://bugs.kde.org/show_bug.cgi?id=376943	Guides should be editable with precision by allowing user to input the new value, and they shouldn't only use pixels as units
- [ ] 401496	https://bugs.kde.org/show_bug.cgi?id=401496	Brush sensor curves: quick copy/paste buttons request
- [ ] 425554	https://bugs.kde.org/show_bug.cgi?id=425554	[Request] Trim layer(s) to selection

#### Layer Stack
- [ ] 380141	https://bugs.kde.org/show_bug.cgi?id=380141	Make it possible to merge masks

#### Tools
- [ ] 352221	https://bugs.kde.org/show_bug.cgi?id=352221	Assignable Stabilization/Smoothing Presets
- [ ] 395231	https://bugs.kde.org/show_bug.cgi?id=395231	Pick color from "current and below layers" and "current and below layers without filters"

#### Tools/Transform
- [ ] 371550	https://bugs.kde.org/show_bug.cgi?id=371550	Allow the transform tool to work on multiple selected layers

#### Usability
- [ ] 415494	https://bugs.kde.org/show_bug.cgi?id=415494	[Feature request] Add a value to settings for changing discrete steps for canvas zoom, canvas rotate, brush size.


---------------------------------

## High priority, medium:

#### Animation
- [ ] 369503	https://bugs.kde.org/show_bug.cgi?id=369503	Render animation encode resume after error,by looking at folder
- [ ] 393140	https://bugs.kde.org/show_bug.cgi?id=393140	Waveform in animation track
- [ ] 411109	https://bugs.kde.org/show_bug.cgi?id=411109	Add a way to render animation from prerendered image sequence

#### Brush Engine/Shape
- [ ] 319446	https://bugs.kde.org/show_bug.cgi?id=319446	Use SVG brush file as shape library for animated brush. (Alchemy pull shapes feature)

#### Brush engines
- [ ] 336731	https://bugs.kde.org/show_bug.cgi?id=336731	Saving color information in brush presets
- [ ] 375155	https://bugs.kde.org/show_bug.cgi?id=375155	Add option to texture each dab instead of stroke
- [ ] 382110	https://bugs.kde.org/show_bug.cgi?id=382110	Fill tool does not work if brush eraser is selected

#### Dockers
- [ ] 332040	https://bugs.kde.org/show_bug.cgi?id=332040	Selection Bag Docker.
- [ ] 395449	https://bugs.kde.org/show_bug.cgi?id=395449	Show used tool and/or brush in undo history entries

#### Filter Layers
- [ ] 338002	https://bugs.kde.org/show_bug.cgi?id=338002	Arrange for layers

#### Filters
- [ ] 390822	https://bugs.kde.org/show_bug.cgi?id=390822	Color adjustment curves - no value picking from canvas

#### General
- [ ] 333167	https://bugs.kde.org/show_bug.cgi?id=333167	GIMP 2.10 like high quality scaling algorithms
- [ ] 376942	https://bugs.kde.org/show_bug.cgi?id=376942	Grids don't take cm , mm, in as input, and are only pixel based

#### Tool/Text
- [ ] 391798	https://bugs.kde.org/show_bug.cgi?id=391798	Improve UX for Text Editor

#### Tools
- [ ] 388006	https://bugs.kde.org/show_bug.cgi?id=388006	Tool for creating manga\comics grids
- [ ] 407403	https://bugs.kde.org/show_bug.cgi?id=407403	Transformation and Mirror mode

#### Tools/Freehand
- [ ] 446787	https://bugs.kde.org/show_bug.cgi?id=446787	Implementing a new brush smoothing algorithm simply by up sampling and window averaging

#### Tools/Reference Images
- [ ] 396155	https://bugs.kde.org/show_bug.cgi?id=396155	Reference images tool lacks features

#### Usability
- [ ] 335953	https://bugs.kde.org/show_bug.cgi?id=335953	[Brush-Presets-Docker]  Custom/Manual Sorting  ( feature request )
- [ ] 357493	https://bugs.kde.org/show_bug.cgi?id=357493	Remember color history after reopening a document
- [ ] 383587	https://bugs.kde.org/show_bug.cgi?id=383587	change axis of transform tool without changing the selection/image
- [ ] 397559	https://bugs.kde.org/show_bug.cgi?id=397559	Snapping is sloppy and unpredictable


---------------------------------

## Medium priority, easy:

#### Animation
- [ ] 437021	https://bugs.kde.org/show_bug.cgi?id=437021	Set image type before Composition Export (Jpg, PNG, Alpha...)

#### Brush Engine/Bristle
- [ ] 395798	https://bugs.kde.org/show_bug.cgi?id=395798	Density in Bristle brush isn't pressure sensitive parameter and it isn't exposed in Popup Palette

#### General
- [ ] 135189	https://bugs.kde.org/show_bug.cgi?id=135189	Recombining different layers into one image (inverse of image separation)
- [ ] 364350	https://bugs.kde.org/show_bug.cgi?id=364350	Warn users if saving fails because of a lack of disk space
- [ ] 405604	https://bugs.kde.org/show_bug.cgi?id=405604	Choose Settings profile on launch
- [ ] 447893	https://bugs.kde.org/show_bug.cgi?id=447893	Add the same option to export TIFF for the Comic Manager

#### Scripting
- [ ] 390946	https://bugs.kde.org/show_bug.cgi?id=390946	Make metadata accessible through scripting

#### Shortcuts and Canvas Input Settings
- [ ] 399803	https://bugs.kde.org/show_bug.cgi?id=399803	"New View" action missing in "Settings > Configure Krita... > Keyboard Shortcuts"
- [ ] 430692	https://bugs.kde.org/show_bug.cgi?id=430692	search for shortcuts by key

#### Tools/Reference Images
- [ ] 405130	https://bugs.kde.org/show_bug.cgi?id=405130	Reference Images Tool: Color to Alpha and Invert Image built in

#### Usability
- [ ] 433947	https://bugs.kde.org/show_bug.cgi?id=433947	Save several pressure curves


---------------------------------

## Medium priority, medium:

#### * Unknown
- [ ] 475381	https://bugs.kde.org/show_bug.cgi?id=475381	Add a "Tab bars" option t othe Canvas-only settings

#### Brush Engine/Shape
- [ ] 420095	https://bugs.kde.org/show_bug.cgi?id=420095	We should be easily able to choose another unit of mesurement instead of only "px", like "mm", "cm" and "in" at the tool options when drawing shapes

#### Brush engines
- [ ] 340641	https://bugs.kde.org/show_bug.cgi?id=340641	Better accuracy for grid brush
- [ ] 344108	https://bugs.kde.org/show_bug.cgi?id=344108	Feathering option for Experiment brush
- [ ] 358183	https://bugs.kde.org/show_bug.cgi?id=358183	Feature request: support randomness/rotation in clone brush engine
- [ ] 367842	https://bugs.kde.org/show_bug.cgi?id=367842	Smudge Brush Radius could use a better sampling method.
- [ ] 388238	https://bugs.kde.org/show_bug.cgi?id=388238	Extension to the Particle Brush Engine - Add Color Source
- [ ] 426790	https://bugs.kde.org/show_bug.cgi?id=426790	Request : Noise and Density features for both basic and custom brushes
- [ ] 436562	https://bugs.kde.org/show_bug.cgi?id=436562	MyPaint brushes ignores layer alpha locking

#### Color Selectors
- [ ] 432847	https://bugs.kde.org/show_bug.cgi?id=432847	Option for pick colour "eyedropper" to select from all visible layers EXCEPT the current one

#### Color models
- [ ] 393477	https://bugs.kde.org/show_bug.cgi?id=393477	Add color mode type - RYB color wheel

#### Dockers
- [ ] 397432	https://bugs.kde.org/show_bug.cgi?id=397432	The overview docker does not show updates immediately

#### File formats
- [ ] 382993	https://bugs.kde.org/show_bug.cgi?id=382993	32 bit EXR file opening time needs little improvement
- [ ] 400643	https://bugs.kde.org/show_bug.cgi?id=400643	PSD clipping layers not supported
- [ ] 420731	https://bugs.kde.org/show_bug.cgi?id=420731	:Feature Request: Support PAM files (P7)

#### Filter Layers
- [ ] 385845	https://bugs.kde.org/show_bug.cgi?id=385845	Add Film-like curve adjustment to keep constant hue during curves adjustment

#### Filters
- [ ] 341861	https://bugs.kde.org/show_bug.cgi?id=341861	"Low Frequency Even" like filter
- [ ] 393221	https://bugs.kde.org/show_bug.cgi?id=393221	Request for better native Krita noise filter

#### General
- [ ] 385728	https://bugs.kde.org/show_bug.cgi?id=385728	Autotrim to Image Size Option for Selection
- [ ] 428934	https://bugs.kde.org/show_bug.cgi?id=428934	Show thumbnail when hovering over  composition (like layer popups)

#### Layers/Vector
- [ ] 429567	https://bugs.kde.org/show_bug.cgi?id=429567	vector layers don't support filters

#### Shortcuts and Canvas Input Settings
- [ ] 368679	https://bugs.kde.org/show_bug.cgi?id=368679	Several frequently used commands are missing custom shortcuts

#### Tools
- [ ] 265801	https://bugs.kde.org/show_bug.cgi?id=265801	Add a "Create guide(s) from selected shape(s)" feature
- [ ] 324554	https://bugs.kde.org/show_bug.cgi?id=324554	export subtree
- [ ] 331344	https://bugs.kde.org/show_bug.cgi?id=331344	create infinite straight lines
- [ ] 333170	https://bugs.kde.org/show_bug.cgi?id=333170	Ability to save warp presets-FishEye and Inflate,etc.
- [ ] 334515	https://bugs.kde.org/show_bug.cgi?id=334515	Angle Constraints for PolygonalBrushTool and PolygonalSelectionTool
- [ ] 335880	https://bugs.kde.org/show_bug.cgi?id=335880	Hexagon Grid
- [ ] 396798	https://bugs.kde.org/show_bug.cgi?id=396798	Rotate Selection Boxes with Canvas

#### Tools/Vector
- [ ] 400521	https://bugs.kde.org/show_bug.cgi?id=400521	Performing a logical operation on vector objects adds many nodes

#### Usability
- [ ] 318027	https://bugs.kde.org/show_bug.cgi?id=318027	Apply inherit alpha layer to itself
- [ ] 386019	https://bugs.kde.org/show_bug.cgi?id=386019	Brushes displayed in brush editor may have a lower quality than strokes actually painted on the canvas

