#!/bin/bash


# update databases


# list of arguments expected in the input
optstring="hbw"

WISHES=""
WISHESLINK="https://bugs.kde.org/buglist.cgi?bug_severity=wishlist&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=RESOLVED&bug_status=NEEDSINFO&bug_status=VERIFIED&bug_status=CLOSED&columnlist=product%2Ccomponent%2Cassigned_to%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Cvotes%2Cdupecount&f0=OP&f1=OP&f2=product&f3=component&f4=alias&f5=short_desc&f7=content&f8=CP&f9=CP&j1=OR&limit=0&list_id=1832512&o2=substring&o3=substring&o4=substring&o5=substring&o7=matches&product=krita&query_format=advanced&v2=krita&v3=krita&v4=krita&v5=krita&v7=%22krita%22&ctype=csv&human=1"
#WISHESLINK2="https://bugs.kde.org/buglist.cgi?bug_severity=wishlist\&bug_status=UNCONFIRMED\&bug_status=CONFIRMED\&bug_status=ASSIGNED\&bug_status=REOPENED\&bug_status=RESOLVED\&bug_status=NEEDSINFO\&bug_status=VERIFIED\&bug_status=CLOSED\&columnlist=product%2Ccomponent%2Cassigned_to%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Cvotes%2Cdupecount\&f0=OP\&f1=OP\&f2=product\&f3=component\&f4=alias\&f5=short_desc\&f7=content\&f8=CP\&f9=CP\&j1=OR\&limit=0\&list_id=1832512\&o2=substring\&o3=substring\&o4=substring\&o5=substring\&o7=matches\&product=krita\&query_format=advanced\&v2=krita\&v3=krita\&v4=krita\&v5=krita\&v7=%22krita%22\&ctype=csv\&human=1"


BUGS=""
BUGSLINK="https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=RESOLVED&bug_status=NEEDSINFO&bug_status=VERIFIED&bug_status=CLOSED&chfield=%5BBug%20creation%5D&chfieldfrom=2014-04-01&chfieldto=Now&f0=OP&f1=OP&f2=product&f3=component&f4=alias&f5=short_desc&f7=content&f8=CP&f9=CP&j1=OR&known_name=kritaconfirmednowishes&limit=0&list_id=1851229&o2=substring&o3=substring&o4=substring&o5=substring&o7=matches&product=krita&query_format=advanced&v2=krita&v3=krita&v4=krita&v5=krita&v7=%22krita%22&ctype=csv&human=1"


SCRIPTDIR=`dirname $0`
TMPDIR="$SCRIPTDIR/tmp/"


while getopts ${optstring} arg; do
  case ${arg} in
    h)
      echo "$0 -hbw"
      echo "\t-b\tupdate bugs database"
      echo "\t-w\tupdate wishes database"
      echo "\t-h\tshow this help text"
      echo "\t(You don't need to update both databases)"
      ;;
    w)
       WISHES=1
       ;;
    b)
       BUGS=1
       ;;
    :)
      echo "$0: Must supply an argument to -$OPTARG." >&2
      exit 1
      ;;
    ?)
      echo "Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done



if [ $WISHES ]
then
    echo "Update wishes!"
    wget --output-document=$TMPDIR/tmp_wishes.csv $WISHESLINK
    echo "Output file is $TMPDIR/tmp_wishes.csv"
    wc -l $TMPDIR/tmp_wishes.csv
    $SCRIPTDIR/scripts/updateDatabases.sh -w $TMPDIR/tmp_wishes.csv
fi

if [ $BUGS ]
then
    echo "Update bugs!"
    wget --output-document=$TMPDIR/tmp_bugs.csv $BUGSLINK 
    
    echo "Output file is $TMPDIR/tmp_bugs.csv"
    wc -l $TMPDIR/tmp_bugs.csv
    $SCRIPTDIR/scripts/updateDatabases.sh -b $TMPDIR/tmp_bugs.csv
fi

