# Wishes Manager

The goal of this project is to have an environment that allows me quickly review and mark wishes according to their subjective attributes like how difficult it would be to implement something or how much time it would take.

Due to the popular demand of 100% people using this set of scripts, the database and scripts were adjusted to allow rating both wishes and bugs. Those two databases are separate and can be interacted with separately.

Table of contents:
1. [Requirements](#requirements)
2. [How to use](#howtouse)
3. [Current state](#currentstate)
4. [Rating wishes](#ratingwishes)
5. [Updating the database](#updatingdatabase)
6. [Confirming outdated ratings](#confirming)


## Requirements  <a name="requirements"></a>

You need to have Perl installed. One additional library is required. If you get a similar error to this one:

```
Can't locate Text/CSV.pm in @INC (you may need to install the Text::CSV module) (@INC contains: 
/usr/lib/perl5/5.30/site_perl /usr/share/perl5/site_perl /usr/lib/perl5/5.30/vendor_perl /usr/share/perl5/vendor_perl 
/usr/lib/perl5/5.30/core_perl /usr/share/perl5/core_perl) at ./wishman.pl line 7.
BEGIN failed--compilation aborted at ./wishman.pl line 7.
```

You need to run:

```
cpan Text::CSV
```

to install that library. (You might need replace `cpan` by something else but similar if you're not on Linux, you can refer to perl modules installation guides for your system).

## How to use  <a name="howtouse"></a>

You need to be in the repository directory. All scripts assume that.
For the wishes, just run the script:

```
./wishinteractive.sh
# OR
# ./wishinteractive.sh X, where X - the number of entries you want to rate:
# by default it's 5
./wishinteractive.sh 7
```

To rate bugs instead of wishes use:
```
./buginteractive.sh
```

By default it will show you 5 entries, but if you call it with an argument, it will show you as many entries as you asked for.

It will show you the Bug ID (number), then a link to the page with this bug report, then the title of the bug.

It will ask you directly for every option so you don't have to remember them.

## Current state <a name="currentstate"></a>

![Statistics in bugs and wishes chart](img/all.png "Statistics in bugs and wishes")

## Rating wishes <a name="ratingwishes"></a>

I hope you know a bit about Krita and/or programming, otherwise it might be hard for you to judge wish reports. But I guess if you try, you'll know if you can judge them or not. Just please do not increase the priority of wishes you personally want implemented - of course some bias will always be there, but just try to stay be honest with yourself :)

### Priority

It has four levels, more than all others.
- verylow - reserve it for very rare occassions when it's obvious there is very little gain for a huge amount of work
- low - it's outside of Krita's main goal (drawings, paintings, comics, animations from scratch).
- medium - for all you don't think should be either high or low (it has to be in the Krita's goal).
- high - things you think most artists would like (and must be in Krita's goal).

### Difficulty

- easy - good for beginners in Krita, will teach them a bit of code, suitable to give out to every GSOC wannabe out there.
- medium - a standard-level programmer should be able to do it, if they don't know Krita, maybe with a bit of help, but should be possible.
- high - Dmitry-level ;)

### Time

I know it's hard, but just: a little additional option in some niche part of the code will take only a day or so, while refactoring half of Krita will take forever.

It's different from Difficulty because there are sometimes tedious but easy tasks that could be suitable for beginners as well, just... time-consuming.

- short - a few days max
- medium - two weeks, a few weeks, less than two months
- long - anything that looks like it could take longer than two months

### Ready or needs discussions?

`wishinteractive.sh` script asks: *"Do you think it needs more discussions?"* That option might be confusing, but I noticed that a lot of wish reports are not exactly "confirmed" in a sense that Krita team would love to see the patch with it coming: especially if it's more of a change of behaviour instead of a new feature.

Often reading comments under the wish description will tell you that there is no consensus that the particular change will be beneficial or it's worth the struggle. They need "notready".

On the other hand, often it's obvious that it's just another option or another little feature in some Krita's corner and everyone agrees it would be great if it was, there is just no time to implement it right away. They need "ready".

If you're really unsure what category it belongs to, go with "unsure".

## Updating the database <a name="updating database"></a>

Once in a while the database will have to be updated.


### Automatic (from url)

The easiest way to update the database is to calla script to update wishes or bugs database. (You might want to remove the temporary file afterwards).

```
./updateDatabaseFromUrl -w
```

or


```
./updateDatabaseFromUrl -b
```

This will do everything that needs to be done.

### Manual (from a file)

Alternatively, you can do it manually (from a file on your disk).

1. To get the database file from bugzilla, go to:

    [Bugzilla search wishes link](https://bugs.kde.org/buglist.cgi?bug_severity=wishlist&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=RESOLVED&bug_status=NEEDSINFO&bug_status=VERIFIED&bug_status=CLOSED&columnlist=product%2Ccomponent%2Cassigned_to%2Cbug_status%2Cresolution%2Cshort_desc%2Cchangeddate%2Cvotes%2Cdupecount&f0=OP&f1=OP&f2=product&f3=component&f4=alias&f5=short_desc&f7=content&f8=CP&f9=CP&j1=OR&limit=0&list_id=1728355&o2=substring&o3=substring&o4=substring&o5=substring&o7=matches&product=krita&query_format=advanced&v2=krita&v3=krita&v4=krita&v5=krita&v7=%22krita%22)
    
    [Bugzilla search bugs link](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=RESOLVED&bug_status=NEEDSINFO&bug_status=VERIFIED&bug_status=CLOSED&chfield=%5BBug%20creation%5D&chfieldfrom=2014-04-01&chfieldto=Now&f0=OP&f1=OP&f2=product&f3=component&f4=alias&f5=short_desc&f7=content&f8=CP&f9=CP&j1=OR&known_name=kritaconfirmednowishes&limit=0&list_id=1808556&o2=substring&o3=substring&o4=substring&o5=substring&o7=matches&order=changeddate%2Cproduct%20DESC%2Cresolution%2Cbug_status%2Cpriority%2Cbug_severity&product=krita&query_format=advanced&v2=krita&v3=krita&v4=krita&v5=krita&v7=%22krita%22)

    You need to make sure you took all the wishes there, not only the first 500, as bugzilla usually try.
    


2. Save the csv file.

3. Run the script:

```
./scripts/updateDatabases.sh -b [bugs new database csv file] -w [wishes new database csv file]
```

(You don't need to update both databases at once, just update the one you're interested in by providing the proper option and the proper path).


## Confirming outdated wishes <a name="confirming"></a>

Use either of those scripts. They behave just the same way as `wishinteractive.sh` and `buginteractive.sh`.

```
./confirmwishesinteractive.sh
./confirmbugsinteractive.sh
```





