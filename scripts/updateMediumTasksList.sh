#!/bin/bash


FILE=MediumTasks.md

echo > $FILE

echo "## Tasks for developers wanting a bit more challenge" >> $FILE
echo >> $FILE

echo "If you wish to implement one of the following wishes, please contact Krita team first!" >> $FILE
echo >> $FILE

echo "### Wishes: " >> $FILE
echo >> $FILE


TMP=tmp/tasks.tmp
echo -n > $TMP

./wishman.pl get high medium short ready | sed 's/$/(medium, short)/' >> $TMP
./wishman.pl get high easy medium ready | sed 's/$/(easy, medium)/' >> $TMP
./wishman.pl get high medium medium ready | sed 's/$/(medium, medium)/' >> $TMP

./wishman.pl get medium medium short ready | sed 's/$/(medium, short)/' >> $TMP
./wishman.pl get medium easy medium ready | sed 's/$/(easy, medium)/' >> $TMP
./wishman.pl get medium medium medium ready | sed 's/$/(medium, medium)/' >> $TMP

cat $TMP | uniq | scripts/printPrettyDivided.pl "wishes" >> $FILE


echo "### Bugs: " >> $FILE
echo >> $FILE

echo -n > $TMP

./bugman.pl get high medium short ready | sed 's/$/(medium, short)/' >> $TMP
./bugman.pl get high easy medium ready | sed 's/$/(easy, medium)/' >> $TMP
./bugman.pl get high medium medium ready | sed 's/$/(medium, medium)/' >> $TMP

./bugman.pl get medium medium short ready | sed 's/$/(medium, short)/' >> $TMP
./bugman.pl get medium easy medium ready | sed 's/$/(easy, medium)/' >> $TMP
./bugman.pl get medium medium medium ready | sed 's/$/(medium, medium)/' >> $TMP

cat $TMP | uniq | scripts/printPrettyDivided.pl "bugs" >> $FILE



