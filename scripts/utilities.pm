#!/usr/bin/perl

# utilities module
package utilities;


use Data::Dumper;
use Text::Unidecode;

sub createColumnsHeadersData;
sub getBugLineData;

use utf8;


our $spreadsheetFilenameWishes = "data/wishes.csv";
our $spreadsheetFilenameOriginalWishes = "tmp/wishes-orig.csv";
our $tempFilenameWishes = "tmp/wishes-out.csv";

our $spreadsheetFilenameBugs = "data/bugs.csv";
our $spreadsheetFilenameOriginalBugs = "tmp/bugs-orig.csv";
our $tempFilenameBugs = "tmp/bugs-out.csv";

our $logFilename = "changes.log";




our $bugIdHeader = "Bug ID";
our $summaryHeader = "Summary";
our $changedHeader = "Changed";
our $componentHeader = "Component";


our $priorityHeader = "Priority";
our $difficultyHeader = "Difficulty";
our $timeHeader = "EstimatedTime";
our $readyHeader = "Ready";
our $lastRatedHeader = "LastRated";



our $assigneeHeader = "Assignee";
our $assigneeRealName = "Assignee Real Name";
our $reporterHeader = "Reporter";
our $reporterRealName = "Reporter Real Name";

our @unwantedColumns = ($assigneeHeader, $assigneeRealName, $reporterHeader, $reporterRealName);

our $statusHeader = "Status";
our $resolutionHeader = "Resolution";

our $wishgroupHeader = "Wishgroup";




sub addCustomColumn
{
	my ($columnData, $columnName) = @_;
	
	if(!defined $columnData->{$columnName})
	{
		my $num = keys %$columnData;
		$columnData->{$columnName} = {"id" => $num, "name" => $columnName};
	}
	
	return $columnData;
}


sub createColumnsHeadersData
{
	my ($csvParser, $line) = @_;
	my @columns = $line;
	
	# "Bug ID","Product","Component","Assignee","Status","Resolution","Summary","Changed"
	$csvParser->parse($line);
	my @fields = $csvParser->fields();
	
	my $columnData = {};
	
	for my $i (0..$#fields)
	{
		my $name = $fields[$i];
		$columnData->{$name} = {"id" => $i, "name" => $name};
	}
	
	addCustomColumn($columnData, $priorityHeader);
	addCustomColumn($columnData, $difficultyHeader);
	addCustomColumn($columnData, $timeHeader);
	addCustomColumn($columnData, $readyHeader);
	addCustomColumn($columnData, $lastRatedHeader);
	
	
	#print STDERR Dumper($columnData);
	
	for my $key (keys %$columnData)
	{
		#print STDERR "key = " . $key . " column data = " . $columnData->{$key}{"id"} . " " . $columnData->{$key}{"name"} . "\n";
	}
	
	return $columnData;
	
}


sub getColumnsHeadersLine
{
	my ($csvParser, $columnsData) = @_;
	
	my @fields = ();
	my $num = keys %$columnsData;
	
	#print STDERR "########\n";
	
	#print STDERR Dumper($columnsData);
	
	for my $i (0..($num - 1))
	{
		my $column = getCorrectColumnHeader($columnsData, $i);
		if(!defined $column)
		{
			$column = "";
		}
		push @fields, $column;
	}
	
	#print STDERR Dumper(@fields);
	
	my $status = $csvParser->combine(@fields);
	my $response = $csvParser->string();
	
	#print STDERR "########^^^\n";
	
	return $response;
}



sub getCorrectColumnHeader
{
	my ($columnsData, $id) = @_;
	
	for my $key (keys %$columnsData)
	{
		if($columnsData->{$key}{"id"} == $id)
		{
			return $columnsData->{$key}{"name"};
		}
	}
	
	print STDERR "Not found: $id";
	print STDERR "in " . Dumper($columnsData);
	return undef;
}

sub sanitizeLine
{
	my ($line) = @_;
	
	$line =~ s|^[:ascii:]|_|g;
	$line =~ s|＼|_|g;
	
	return $line;
}



sub getBugLineData
{
	#print "WHATTHEHELL!!!\n";
	#print STDERR "WHATTHEHELL!!!\n";
	my ($csvParser, $columnsData, $line) = @_;
	
	$line = sanitizeLine($line);
	
	
	$csvParser->parse($line);
	my @fields = $csvParser->fields();
	
	
	# "Bug ID","Product","Component","Assignee","Status","Resolution","Summary","Changed"
	
	# add new columns if needed
	#push @fields, ('', '', '', '') if @fields < 11;
	#push @fields, ('') if @fields < 12;
	
	my $bugData = {};
	
	
	#print STDERR "number of fields: " . $#fields;
	
	#print STDERR Dumper(@utilities::unwantedColumns);
	
	
	for my $i (0..$#fields)
	{
		#print STDERR "what's going on?\n";
		my $column = getCorrectColumnHeader($columnsData, $i);
		#print STDERR "column = $column\n";
		if (grep /$column/,@utilities::unwantedColumns) 
		{ 
			#print STDERR "so we're gonna change it to redacted!!!\n";
			$bugData->{$column} = "[REDACTED]";
		} 
		else 
		{
			#print STDERR "not going to change...\n";
			$bugData->{$column} = $fields[$i];
		}
	}
	
	#print STDERR "get bug line data " . Dumper($bugData);
	
	
	
	#my $bugData = {
		## original
		#'bugNo' => $fields[0],
		#'product' => $fields[1],
		#'component' => $fields[2],
		#'assignee' => $fields[3],
		#'status' => $fields[4],
		#'resolution' => $fields[5],
		#'title' => $fields[6],
		#'changed' => $fields[7],
		
		## new fields
		#'priority' => $fields[8],
		#'difficulty' => $fields[9],
		#'time' => $fields[10],
		#'ready' => $fields[11]
	#};
	return $bugData;
	
}

sub getBugLineFromData
{
	my ($csvParser, $columnsData, $bugData) = @_;
	
	my $num = keys %$columnsData;
	
	my @fields = ();
	
	#print STDERR Dumper($bugData);
	#print STDERR Dumper($columnsData);
	
	
	for my $i (0..($num - 1))
	{
		#print STDERR "column num in bug line from data: $i\n";	
		push @fields, $bugData->{utilities::getCorrectColumnHeader($columnsData, $i)};
	}
	
	my $status = $csvParser->combine(@fields);
	my $response = $csvParser->string();
	
	#print STDERR "bug line from data: $reponse\n";
	
	return $response;
	
	
}

sub isTextVariableEmpty
{
	my ($bugVar) = @_;
	return !defined $bugVar || $bugVar eq '';
}


sub checkSolved
{
	my ($bug) = @_;
	
	return 0 if($bug->{$utilities::statusHeader} eq "REPORTED");
	return 0 if($bug->{$utilities::statusHeader} eq "CONFIRMED");
	return 0 if($bug->{$utilities::statusHeader} eq "REOPENED");
	return 0 if($bug->{$utilities::statusHeader} eq "NEEDSINFO") && ($bug->{$utilities::resolutionHeader} eq "WAITINGFORINFO");
	
	return 1;
	
}


sub isRated
{
	my ($bug) = @_;
	
	return 1 if(!isTextVariableEmpty($bug->{$utilities::priorityHeader}));
	return 0;
}


################################
1;
