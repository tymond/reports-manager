#!/usr/bin/perl


use warnings;
use strict;


my $skippedIssuesFile = "";
my %skippedIssuesList;

if ($#ARGV >= 0) {
    $skippedIssuesFile = $ARGV[0];
    if (open(my $skippedIssues, "<:encoding(UTF-8)", $skippedIssuesFile))
    {
        while (my $line = <$skippedIssues>)
        {
            chomp $line;
            my @parted = split('\t', $line);
            next if $#parted < 0;
            my $issueNum = $parted[0];
            $skippedIssuesList{$issueNum} = 1;
        }
    }
}

while (my $line = <stdin>)
{
    chomp $line;
    my @parted = split('\t', $line);
    next if $#parted < 0;
    my $issueNum = $parted[0];
    
    if (exists $skippedIssuesList{$issueNum})
    {
        next;
    }
    else
    {
        print $line . "\n";
    }
    
}
