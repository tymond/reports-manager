#!/usr/bin/env bash



# create charts data

FILE="tmp/chartsData.txt"

echo -n > $FILE;
### ALL



ALL=`./wishman.pl get "" "" "" "" | wc -l`
EMPTY=`./wishman.pl empty | wc -l`
RATED=`scripts/countAllRatedWishes.sh | head -n1 | cut -f2 -d' '`

echo "[$ALL | $RATED | $EMPTY]" > /dev/stderr

echo "img/all_wishes.svg" >> $FILE;
echo -e "\tall\trated\twaiting\t" >> $FILE;
echo -e "0\t$ALL\t$RATED\t$EMPTY\t0" >> $FILE;
echo -e "###" >> $FILE;


ALL=`./bugman.pl get "" "" "" "" | wc -l`
EMPTY=`./bugman.pl empty | wc -l`
RATED=`scripts/countAllRatedWishes.sh | tail -n1 | cut -f2 -d' '`

echo "[$ALL | $RATED | $EMPTY]" > /dev/stderr

echo "img/all_bugs.svg" >> $FILE;
echo -e "\tall\trated\twaiting\t" >> $FILE;
echo -e "0\t$ALL\t$RATED\t$EMPTY\t0" >> $FILE;
echo -e "###" >> $FILE;




### PRIORITIES
VERYLOW=`./wishman.pl get verylow "" "" "" | wc -l`
LOW=`./wishman.pl get low "" "" "" | wc -l`
MEDIUM=`./wishman.pl get medium "" "" "" | wc -l`
HIGH=`./wishman.pl get high "" "" "" | wc -l`


echo "img/priorities_wishes.svg" >> $FILE;
echo -e "\tverylow\tlow\tmedium\thigh\t" >> $FILE;
echo -e "0\t$VERYLOW\t$LOW\t$MEDIUM\t$HIGH\t0" >> $FILE;
echo -e "###" >> $FILE;


VERYLOW=`./bugman.pl get verylow "" "" "" | wc -l`
LOW=`./bugman.pl get low "" "" "" | wc -l`
MEDIUM=`./bugman.pl get medium "" "" "" | wc -l`
HIGH=`./bugman.pl get high "" "" "" | wc -l`


echo "img/priorities_bugs.svg" >> $FILE;
echo -e "\tverylow\tlow\tmedium\thigh\t" >> $FILE;
echo -e "0\t$VERYLOW\t$LOW\t$MEDIUM\t$HIGH\t0" >> $FILE;
echo -e "###" >> $FILE;

### READINESS
READY=`./wishman.pl get "" "" "" ready | wc -l`
UNSURE=`./wishman.pl get "" "" "" unsure | wc -l`
NOTREADY=`./wishman.pl get "" "" "" notready | wc -l`


echo "img/readiness_wishes.svg" >> $FILE;
echo -e "\tready\tunsure\tnotready\t" >> $FILE;
echo -e "0\t$READY\t$UNSURE\t$NOTREADY\t0" >> $FILE;
echo -e "###" >> $FILE;


READY=`./bugman.pl get "" "" "" ready | wc -l`
UNSURE=`./bugman.pl get "" "" "" unsure | wc -l`
NOTREADY=`./bugman.pl get "" "" "" notready | wc -l`


echo "img/readiness_bugs.svg" >> $FILE;
echo -e "\tready\tunsure\tnotready\t" >> $FILE;
echo -e "0\t$READY\t$UNSURE\t$NOTREADY\t0" >> $FILE;
echo -e "###" >> $FILE;


#######

scripts/createChartsFromData.pl


inkscape -z -e img/all_wishes.png -h 500 img/all_wishes.svg #2> /dev/null > /dev/null
inkscape -z -e img/all_bugs.png -h 500 img/all_bugs.svg #2> /dev/null > /dev/null

inkscape -z -e img/priorities_wishes.png -h 500 img/priorities_wishes.svg 2> /dev/null > /dev/null
inkscape -z -e img/priorities_bugs.png -h 500 img/priorities_bugs.svg 2> /dev/null > /dev/null

inkscape -z -e img/readiness_wishes.png -h 500 img/readiness_wishes.svg 2> /dev/null > /dev/null
inkscape -z -e img/readiness_bugs.png -h 500 img/readiness_bugs.svg 2> /dev/null > /dev/null


convert img/all_wishes.png img/all_bugs.png +append img/all.png

