#!/bin/bash


FILE=BeginnersTasks.md

echo > $FILE

echo "## Tasks for relative beginners" >> $FILE
echo >> $FILE

echo "If you wish to implement one of the following wishes, please contact Krita team first!" >> $FILE
echo >> $FILE

echo "### Wishes" >> $FILE

TMPFILE=tmp/easy.tmp

echo -n > $TMPFILE
./wishman.pl get high easy short ready >> $TMPFILE
./wishman.pl get medium easy short ready >> $TMPFILE


cat $TMPFILE | scripts/printPrettyDivided.pl "wishes" >> $FILE

echo "Wishes:"
echo `cat $TMPFILE | wc -l`

echo "### Bugs" >> $FILE

echo -n > $TMPFILE
./bugman.pl get high easy short ready >> $TMPFILE
./bugman.pl get medium easy short ready >> $TMPFILE


cat $TMPFILE | scripts/printPrettyDivided.pl "bugs" >> $FILE

echo "Bugs:"
echo `cat $TMPFILE | wc -l`
