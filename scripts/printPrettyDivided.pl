#!/usr/bin/perl

use warnings;
use strict;

use File::Copy;

use Text::CSV;
use Data::Dumper;
use POSIX qw/strftime/;

binmode STDOUT, ':utf8';

use lib '.';
use lib 'scripts/';
use utilities;



## FUNCTIONS ##
sub execute;
sub closeOnError;
sub closeOnErrorUsage;


sub executeLine;
sub executePut;
sub executeGet;

sub getCommand;
sub getArguments;
sub getBugLineData;
sub getBugLineFromData;

sub createBugLink;


## GLOBAL VARIABLES ##

my $csvParser = Text::CSV->new({ sep_char => ',' });

my $spreadsheetFilename;
my $spreadsheetFilenameOriginal;
my $tempFilename;

if($ARGV[0] ne "bugs" && $ARGV[0] ne "wishes")
{
	die "It takes an argument: either 'bugs' or 'wshes'\n";
	
}


if($ARGV[0] eq "bugs")
{
	$spreadsheetFilename = $utilities::spreadsheetFilenameBugs;
	$spreadsheetFilenameOriginal = $utilities::spreadsheetFilenameOriginalBugs;
	$tempFilename = $utilities::tempFilenameBugs;
}
else
{
	$spreadsheetFilename = $utilities::spreadsheetFilenameWishes;
	$spreadsheetFilenameOriginal = $utilities::spreadsheetFilenameOriginalWishes;
	$tempFilename = $utilities::tempFilenameWishes;
}


copy($spreadsheetFilename, $spreadsheetFilenameOriginal);

execute();

copy($tempFilename, $spreadsheetFilename);

sub execute
{
	my $bugIds = getBugIds();
	#print STDERR Dumper($bugIds);
	
	my $divided = getDividedByComponents($bugIds);
	
	prettyPrint($divided);


}

sub getBugIds
{
	my $bugIds = {};

	while(my $line = <STDIN>)
	{
		chomp $line;

		my @fields = split('\t', $line);
		my $bugNo = $fields[0];

		$bugIds->{$bugNo} = "";

	}
	return $bugIds;
	
}

sub getDividedByComponents
{
	my ($bugIds) = @_;
	
	my $columnsData = {};
	my $divided = {};
	
	open(my $in, "<:encoding(UTF-8)", $spreadsheetFilename) or die "Cannot open file $spreadsheetFilename for reading.";
	open(my $out, ">:encoding(UTF-8)", $tempFilename) or die "Cannot open file $tempFilename for writing.";
	
	my $lineNo = 0;
	
	
	while(my $line = <$in>)
	{
		chomp $line;
		
		if($lineNo == 0)
		{
			$columnsData = utilities::createColumnsHeadersData($csvParser, $line);
			print $out utilities::getColumnsHeadersLine($csvParser, $columnsData) . "\n";
			$lineNo += 1;
			next;
		}
		
		my $lineNew = executeLine($line, $columnsData, $bugIds, $divided);
		
		print $out $lineNew . "\n";
		$lineNo += 1;
	}
	
	close($in);
	close($out);
	
	#print STDERR Dumper($divided);
	return $divided;
	
}

sub executeLine
{
	#print STDERR "???";
	my ($line, $columnsData, $bugIds, $divided) = @_;
	
	my $bugData = utilities::getBugLineData($csvParser, $columnsData, $line);
	if(defined $bugIds->{$bugData->{$utilities::bugIdHeader}})
	{
		#print STDERR "exists!!!";
		if(exists $divided->{$bugData->{$utilities::componentHeader}})
		{
			push @{$divided->{$bugData->{$utilities::componentHeader}}}, $bugData;
		}
		else
		{
			$divided->{$bugData->{$utilities::componentHeader}} = [$bugData];
		}
	}
	
	return $line;
	
}


sub prettyPrint
{
	my ($divided) = @_;
	#print STDERR Dumper($divided);
	for my $key (sort keys %{$divided})
	{
		print "#### $key\n";
		my @array = @{$divided->{$key}};
		for my $i (0..$#array)
		{
			my $bugData = $array[$i];
			my $bugLine = prettyPrintBug($bugData);
			print "$bugLine\n"
		}
		print "\n";
	}
	
}


sub prettyPrintBug
{
	my ($bugData) = @_;
	
	return "- [ ] $bugData->{$utilities::bugIdHeader}\thttps://bugs.kde.org/show_bug.cgi?id=$bugData->{$utilities::bugIdHeader}\t$bugData->{$utilities::summaryHeader}";
}
