#!/usr/bin/perl

use warnings;
use strict;

use lib './scripts/';
use utilities;
use updateDatabaseReports;


updateDatabaseReports::executeEverything($ARGV[0], $utilities::spreadsheetFilenameBugs, 
	$utilities::spreadsheetFilenameOriginalBugs, $utilities::tempFilenameBugs);
