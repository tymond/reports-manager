#!/bin/bash

echo "update small gsoc tasks"


FILE=SmallGSOCTasks.md

echo > $FILE

echo "# Tasks for small GSOC" >> $FILE
echo >> $FILE

echo "This list is mostly for internal use: as an auto-generated list of ideas that can be used for ideas for GSOC students in 2021 \
and future years, since GSOC now is smaller than in previous years" >> $FILE
echo >> $FILE

echo "If you wish to implement one of the following wishes, please contact Krita team first!" >> $FILE
echo >> $FILE




echo "## High priority, easy:" >> $FILE
echo >> $FILE


TMP=tmp/tasks.tmp

echo -n > $TMP
./wishman.pl get high easy medium ready | sed 's/$/(easy, medium)/' >> $TMP
cat $TMP | uniq | scripts/printPrettyDivided.pl "wishes" >> $FILE

echo >> $FILE
echo "---------------------------------" >> $FILE
echo >> $FILE

echo "## High priority, medium:" >> $FILE
echo >> $FILE

echo -n > $TMP
./wishman.pl get high medium medium ready | sed 's/$/(medium, medium)/' >> $TMP
cat $TMP | uniq | scripts/printPrettyDivided.pl "wishes" >> $FILE

echo >> $FILE
echo "---------------------------------" >> $FILE
echo >> $FILE

echo "## Medium priority, easy:" >> $FILE
echo >> $FILE

echo -n > $TMP
./wishman.pl get medium easy medium ready | sed 's/$/(easy, medium)/' >> $TMP
cat $TMP | uniq | scripts/printPrettyDivided.pl "wishes" >> $FILE

echo >> $FILE
echo "---------------------------------" >> $FILE
echo >> $FILE

echo "## Medium priority, medium:" >> $FILE
echo >> $FILE

echo -n > $TMP
./wishman.pl get medium medium medium ready | sed 's/$/(medium, medium)/' >> $TMP
cat $TMP | uniq | scripts/printPrettyDivided.pl "wishes" >> $FILE





#./wishman.pl get high medium short ready | sed 's/$/(medium, short)/' >> $TMP
./wishman.pl get high easy medium ready | sed 's/$/(easy, medium)/' >> $TMP
./wishman.pl get high medium medium ready | sed 's/$/(medium, medium)/' >> $TMP

#./wishman.pl get medium medium short ready | sed 's/$/(medium, short)/' >> $TMP
./wishman.pl get medium easy medium ready | sed 's/$/(easy, medium)/' >> $TMP
./wishman.pl get medium medium medium ready | sed 's/$/(medium, medium)/' >> $TMP

#./wishman.pl get low easy medium ready | sed 's/$/(low, easy, medium)/' >> $TMP
#./wishman.pl get low medium medium ready | sed 's/$/(low, medium, medium)/' >> $TMP

#cat $TMP | uniq | wc -l

#cat $TMP | uniq | scripts/printPrettyDivided.pl "wishes" >> $FILE





