#/bin/bash


#Just a simple script

EASY=`./wishman.pl get "" "easy" "" | wc -l`
MED=`./wishman.pl get "" "medium" "" | wc -l`
DIFF=`./wishman.pl get "" "difficult" "" | wc -l`

ALL=$(($EASY + $MED + $DIFF))
echo "Wishes: $ALL"

EASY=`./bugman.pl get "" "easy" "" | wc -l`
MED=`./bugman.pl get "" "medium" "" | wc -l`
DIFF=`./bugman.pl get "" "difficult" "" | wc -l`

ALL=$(($EASY + $MED + $DIFF))
echo "Bugs: $ALL"
