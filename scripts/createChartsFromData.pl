#!/usr/bin/env perl


use warnings;
use strict;
use SVGGraph;
use Data::Dumper;
#use GD::Graph;
use Chart::Gnuplot;


my $dataFile = "tmp/chartsData.txt";

my $separator = "###";




open(my $fh, "<", $dataFile) or die "Cannot open data file: $dataFile\n";


my $titleLine = 1;#"title";
my $headerLine = 2;#"header";
my $dataLine = 3;#"data";


my $lineMeaning = $titleLine;


my $chartData = {};
my $allCharts = [];


while(my $line = <$fh>)
{
	chomp $line;
	
	if($lineMeaning == $titleLine)
	{
		$chartData->{$titleLine} = $line;
		
		$lineMeaning = $headerLine;
	}
	elsif($lineMeaning == $headerLine)
	{
		my $headers = [split("\t", $line)];
		$chartData->{$headerLine} = $headers;
		$lineMeaning = $dataLine;
	}
	elsif($lineMeaning == $dataLine)
	{
		if($line =~ /$separator/)
		{
			$lineMeaning = $titleLine;
			push @$allCharts, $chartData;
			$chartData = {}; 
			next;
		}
		
		my $data = [split("\t", $line)];
		
		if(!defined $chartData->{$dataLine})
		{
			$chartData->{$dataLine} = [];
		}
		push @{$chartData->{$dataLine}}, $data;
		
	}
	
}



for my $i (0..$#{$allCharts})
{
	my $currChartData = $allCharts->[$i];
	
	my $ffh;
	my $title = $currChartData->{$titleLine};
	open($ffh, ">", $title) or die "Cannot open [$currChartData->{$titleLine}]\n";
	
	
	my $array = [10000..(10000 + $#{$currChartData->{$dataLine}[0]})];
	#my $array = $currChartData->{$headerLine};
	#print STDERR "\$array = \[@{$array}\]\n";
	
	my $data = $currChartData->{$dataLine}[0];
	
	my $SVGGraph = new SVGGraph;
	

	my $svg = SVGGraph->CreateGraph(
                      {'title' => $title,
						  'graphtype' => 'verticalbars', ### verticalbars or spline
						  'barwidth' => 8, ### Width of the bar or dot in pixels
						  'imageheight' => 300, ### The total height of the whole svg image
						  'titlestyle' => 'font-size:12',
						  'horiunitdistance' => 35, ### This is the distance in pixels between 1 x-unit
						  },
                      [$array, $data, '', 'black']
                    );
                    
    
	my $fixed = fixSVG($svg, $currChartData->{$headerLine}, "");
	print $ffh $fixed;
	
	close($ffh);

}
	

sub fixLabels
{
	my ($svg, $xlabels) = @_;
	
	my $i = 0;
	
	my $fixed = "";
	
	my @lines = split(/\n/, $svg);
	
	#print STDERR Dumper(@lines);
	
	
	for my $j (0..$#lines)
	{
		my $line = $lines[$j];
		my $newLine;
		
		#<text x="0" y="10" class="nx">10000</text>
		if($line =~ /(<.*?class=\"nx\".*?>).*?(<.*?>)/)
		{
			if(defined $xlabels->[$i])
			{
				$newLine = "$1" . "$xlabels->[$i]" . "$2";
			}
			else
			{
				$newLine = "$1" . "$2";
			}
			$i++;
		}
		elsif ($line =~ /^(<svg width=")(\d+)(" height="\d+">)$/)
		{
			my $newWidth = $2*0.8;
			$newLine = $1 . $newWidth . $3;
		}
		else
		{
			#print STDERR "doesn't fit: $line\n";
			$newLine = $line;
		}
		$fixed .= "$newLine\n";
	}
	
	
	$svg = $fixed;
	
	return $svg;
	
	
	
}



sub fixSVG
{
	my ($svg, $xlabels, $columnsNum) = @_;
	
	my $fixed = fixLabels($svg, $xlabels);
	
	
	
	return $fixed;
	
}






