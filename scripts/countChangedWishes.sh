#!/bin/bash

echo -n "Staged wishes: "
git diff --staged data/wishes.csv | grep "^+" | grep -v "+++" | wc -l
echo -n "Not staged wishes: "
git diff data/wishes.csv | grep "^+"  | grep -v "+++" | wc -l

echo -n "Staged bugs: "
git diff --staged data/bugs.csv | grep "^+" | grep -v "+++" | wc -l
echo -n "Not staged bugs: "
git diff data/bugs.csv | grep "^+"  | grep -v "+++" | wc -l
