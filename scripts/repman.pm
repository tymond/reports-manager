#!/usr/bin/perl

package repman;

use warnings;
use strict;
use File::Copy;

use Text::CSV;
use Data::Dumper;
use POSIX qw/strftime/;

use lib '.';
use utilities;
use diagnostics;

binmode STDOUT, ':utf8';


# Usage:
# # [command] [..other options]
# put [bug number] [priority] [difficulty] [time]
# get [bug number]
# get [priority] [difficulty] [time]


## FUNCTIONS ##
sub execute;
sub closeOnError;
sub closeOnErrorUsage;


sub executeLine;
sub executePut;
sub executeGet;

sub getCommand;
sub getArguments;
sub getBugLineData;
sub getBugLineFromData;

sub createBugLink;


sub executeEverything;


## GLOBAL VARIABLES ##





my $put = "put";
my $get = "get";
my $cmd = "cmd";
my $help = "help";
my $empty = "empty";
my $gethint = "gethint"; # from Wishgroup
my $outdated = "outdated"; # to get wishes that were rated before last changes
my $confirm = "confirm";
my $gettoconfirm = "gettoconfirm";
my $emptyreported = "emptyreported";

my $neverDate = "2000-01-01 00:00:00";


my $csvParser = Text::CSV->new({ sep_char => ',' });


my @allowedPriorities = ("verylow", "low", "medium", "high");
my @allowedTime = ("short", "medium", "long");
my @allowedDifficulty = ("easy", "medium", "difficult");
my @allowedReadiness = ("ready", "unsure", "notready");

my $usage = "Usage:\
# [command] [..other options]\
$0 put [bug number] [priority] [difficulty] [time]\
$0 get [bug number]\
$0 get [priority] [difficulty] [time]\
$0 empty [how many entries]\
$0 emptyreported [how many entries]\
$0 gettoconfirm [bug number]\
$0 confirm [bug number]\
$0 outdated\
priorities: \t@allowedPriorities\
time: \t\t@allowedTime\
difficulty: \t@allowedDifficulty\
readiness: \t@allowedReadiness\n\n";


#my $spreadsheetFilenameOriginal;
#my $spreadsheetFilename;




sub executeEverything
{
	my ($spreadsheetFilename2, $spreadsheetFilenameOriginal2, $tempFilename) = @_;
	
	my $spreadsheetFilename = $spreadsheetFilename2;
	my $spreadsheetFilenameOriginal = $spreadsheetFilenameOriginal2;
	
	
	

	my $numOfLinesPrintedOut = 0;


	## START ##

	if(@ARGV < 1 || $ARGV[0] eq $help || $ARGV[0] eq "--help" || $ARGV[0] eq "-h") 
	{
		print STDERR $usage;
		exit(0);
	}
	
	open(my $logfile, ">>:encoding(UTF-8)", $utilities::logFilename) or die "Cannot open file [$utilities::logFilename] for appending.";
	print $logfile "@ARGV\n";
	
	copy($spreadsheetFilename, $spreadsheetFilenameOriginal);

	execute($spreadsheetFilename, $tempFilename, @ARGV);

	copy($tempFilename, $spreadsheetFilename);

}





## FUNCTION IMPLEMENTATIONS ##

sub execute 
{
	my $columnsData = {};
	
	my ($spreadsheetFilename, $tempFilename, @commands) = @_;
	
	
	#my ($command, $bugNo, $prio, $diffi, $time, $ready) = getArguments(@commands);
	my ($command) = getCommand(@commands);
	
	
	open(my $in, "<:encoding(UTF-8)", $spreadsheetFilename) or die "Cannot open file [$spreadsheetFilename] for reading.";
	open(my $out, ">:encoding(UTF-8)", $tempFilename) or die "Cannot open file [$tempFilename] for writing.";
	
	my $lineNo = 0;
	
	
	while(my $line = <$in>)
	{
		chomp $line;
		
		if($lineNo == 0)
		{
			$columnsData = utilities::createColumnsHeadersData($csvParser, $line);
			print $out utilities::getColumnsHeadersLine($csvParser, $columnsData) . "\n";
			$lineNo += 1;
			next;
		}
		
		my $lineNew = executeLine($line, $command, $columnsData);
		
		print $out $lineNew . "\n";
		$lineNo += 1;
	}
	
	close($in);
	close($out);
	
	
		
}


sub getArguments
{
	my @commands = @_;
	
	
	my $command = $commands[0];
	
	my $bugNo = "";
	my $prio = "";
	my $diffi = "";
	my $time = "";
	my $ready = "";
	
	
	if($command eq $put)
	{
		# put [bug number] [priority] [difficulty] [time] [optional: readyness]
		if(@commands < 5)
		{
			closeOnErrorUsage("Too little arguments!");
		}
		
		($bugNo, $prio, $diffi, $time) = @commands[1..4];
		
		if(@commands == 6)
		{
			$ready = $commands[5];
		}
		
	}
	elsif($command eq $get)
	{
		# get [bug number]
		# get [priority] [difficulty] [time]
		if(@commands == 2)
		{
			$bugNo = $commands[1];
		}
		elsif(@commands >= 4)
		{
			($prio, $diffi, $time) = @commands[1..3];
			if(@commands == 5)
			{
				$ready = $commands[4];
			}
		}
	}
	elsif($command eq $empty)
	{
		if(@commands == 2)
		{
			$bugNo = $commands[1];
		}
	}
	else
	{
		closeOnError("Wrong command");
	}
	
	
	
	
	
	
	return $command, $bugNo, $prio, $diffi, $time, $ready;
	
}


sub getCommand
{
	my @commands = @_;
	
	my $command = {};
	$command->{"command"} = $commands[0];
	
	
	my $bugNo = "";
	my $prio = "";
	my $diffi = "";
	my $time = "";
	my $ready = "";
	
	
	if($command->{"command"} eq $put)
	{
		# put [bug number] [priority] [difficulty] [time] [optional: readyness]
		if(@commands < 5)
		{
			print STDERR "Too little arguments!\n";
			print STDERR $usage;
			closeOnError("Too little arguments!");
		}
		
		($bugNo, $prio, $diffi, $time) = @commands[1..4];
		
		if(@commands == 6)
		{
			$ready = $commands[5];
		}
		
		#print STDERR "Command: PUT $bugNo $prio $diffi $time $ready \n";
	}
	elsif($command->{"command"} eq $get)
	{
		# get [bug number]
		# get [priority] [difficulty] [time]
		if(@commands == 2)
		{
			$bugNo = $commands[1];
		}
		elsif(@commands >= 4)
		{
			($prio, $diffi, $time) = @commands[1..3];
			if(@commands == 5)
			{
				$ready = $commands[4];
			}
		}
		#print STDERR "Command: GET $bugNo | $prio $diffi $time $ready\n";
	}
	elsif($command->{"command"} eq $empty)
	{
		if(@commands == 2)
		{
			$bugNo = $commands[1];
		}
	}
	elsif($command->{"command"} eq $emptyreported)
	{
		if(@commands == 2)
		{
			$bugNo = $commands[1];
		}
	}
	elsif($command->{"command"} eq $gethint)
	{
		if(@commands == 2)
		{
			$bugNo = $commands[1];
		}
		else
		{
			closeOnErrorUsage("You need to provide bug id for 'gethint' command.");
		}
	}
	elsif($command->{"command"} eq $outdated)
	{
		# nothing to do
	}
	elsif($command->{"command"} eq $confirm)
	{
		if(@commands == 2)
		{
			$bugNo = $commands[1];
		}
		else
		{
			closeOnErrorUsage("You need to provide bug id for 'confirm' command.");
		}
	}
	elsif($command->{"command"} eq $gettoconfirm)
	{
		if(@commands == 2)
		{
			$bugNo = $commands[1];
		}
		else
		{
			closeOnErrorUsage("You need to provide bug id for 'confirm' command.");
		}
	}
	else
	{
		closeOnErrorUsage("The command used is not one of the allowed ones.");
	}
	
	$command->{$utilities::bugIdHeader} = $bugNo;
	$command->{$utilities::priorityHeader} = $prio;
	$command->{$utilities::difficultyHeader} = $diffi;
	$command->{$utilities::timeHeader} = $time;
	$command->{$utilities::readyHeader} = $ready;
	
	return $command;
	
}



sub executeLine
{
	my ($line, $command, $columnsData) = @_;
	if($command->{"command"} eq $put)
	{
		return executePut($line, $columnsData, $command->{$utilities::bugIdHeader}, $command->{$utilities::priorityHeader}, 
			$command->{$utilities::difficultyHeader}, $command->{$utilities::timeHeader}, $command->{$utilities::readyHeader});
	}
	elsif($command->{"command"} eq $get)
	{
		return executeGet($line, $columnsData, $command->{$utilities::bugIdHeader}, $command->{$utilities::priorityHeader}, 
			$command->{$utilities::difficultyHeader}, $command->{$utilities::timeHeader}, $command->{$utilities::readyHeader});
	}
	elsif($command->{"command"} eq $empty)
	{
		return executeEmpty($line, $columnsData, $command->{$utilities::bugIdHeader}, $command->{$utilities::priorityHeader}, 
			$command->{$utilities::difficultyHeader}, $command->{$utilities::timeHeader}, $command->{$utilities::readyHeader});
	}
	elsif($command->{"command"} eq $emptyreported)
	{
		return executeEmptyReported($line, $columnsData, $command->{$utilities::bugIdHeader}, $command->{$utilities::priorityHeader}, 
			$command->{$utilities::difficultyHeader}, $command->{$utilities::timeHeader}, $command->{$utilities::readyHeader});
	}
	elsif($command->{"command"} eq $gethint)
	{
		return executeGetHint($line, $columnsData, $command);
	}
	elsif($command->{"command"} eq $outdated)
	{
		return executeOutdated($line, $columnsData, $command);
	}
	elsif($command->{"command"} eq $confirm)
	{
		return executeConfirm($line, $columnsData, $command);
	}
	elsif($command->{"command"} eq $gettoconfirm)
	{
		return executeGetToConfirm($line, $columnsData, $command);
	}
	else
	{
		return $line;
	}
}




sub isBugInfoEmpty
{
	my ($bugData) = @_;
	return 
			utilities::isTextVariableEmpty($bugData->{$utilities::priorityHeader}) 
		|| utilities::isTextVariableEmpty($bugData->{$utilities::timeHeader})
		|| utilities::isTextVariableEmpty($bugData->{$utilities::difficultyHeader})
		|| utilities::isTextVariableEmpty($bugData->{$utilities::readyHeader});
}


sub executeEmpty
{
	my ($line, $columnsData, $bugNo, $prio, $diffi, $time, $ready) = @_;
	my $bugData = utilities::getBugLineData($csvParser, $columnsData, $line);

	if(isBugInfoEmpty($bugData) && !utilities::checkSolved($bugData))
	{
		print createBugInfoLine($bugData) . "\n";
	}
	return $line;
}

sub executeEmptyReported
{
	my ($line, $columnsData, $bugNo, $prio, $diffi, $time, $ready) = @_;
	my $bugData = utilities::getBugLineData($csvParser, $columnsData, $line);

	if(isBugInfoEmpty($bugData) && $bugData->{$utilities::statusHeader} eq "REPORTED" && !utilities::checkSolved($bugData))
	{
		print createBugInfoLine($bugData) . "\n";
	}
	return $line;
}


sub getCurrentTimeField
{
	return strftime ("%Y-%m-%d %H:%M:%S", gmtime(time()));
}

sub executePut
{
	my ($line, $columnsData, $bugNo, $prio, $diffi, $time, $ready) = @_;
	
	closeOnError("Priority $prio not from the allowed list: @allowedPriorities") if not ($prio eq '' || grep( /^$prio$/, @allowedPriorities ) );
	closeOnError("Difficulty $diffi not from the allowed list: @allowedDifficulty") if not ($diffi eq '' || grep( /^$diffi$/, @allowedDifficulty ) );
	closeOnError("Time $time not from the allowed list: @allowedTime") if not ($time eq '' || grep( /^$time$/, @allowedTime ) );
	closeOnError("Readiness $ready not from the allowed list: @allowedReadiness") if not ($ready eq '' || grep( /^$ready$/, @allowedReadiness ) );
	
	#my @fields = split(',', $line);
	my $bugData = utilities::getBugLineData($csvParser, $columnsData, $line);
	if($bugData->{$utilities::bugIdHeader} eq $bugNo)
	{
		print createBugInfoLine($bugData) . "\n";
		$bugData->{$utilities::priorityHeader} = $prio;
		$bugData->{$utilities::difficultyHeader} = $diffi;
		$bugData->{$utilities::timeHeader} = $time;
		$bugData->{$utilities::readyHeader} = $ready;
		$bugData->{$utilities::lastRatedHeader} = getCurrentTimeField();
		return utilities::getBugLineFromData($csvParser, $columnsData, $bugData);
	}
	else 
	{
		return $line;
	}
}

sub fitsCondition
{
	my ($condition, $value) = @_;
	
	return 1 if(utilities::isTextVariableEmpty($condition));
	return 0 if(utilities::isTextVariableEmpty($value));
	return $condition eq $value;
}


sub executeGet
{
	my ($line, $columnsData, $bugNo, $prio, $diffi, $time, $ready) = @_;
	
	
	my $bugData = utilities::getBugLineData($csvParser, $columnsData, $line);
	
	my $fitsAll = 0;
	
	if($bugNo ne '')
	{
		if($bugData->{$utilities::bugIdHeader} == $bugNo) 
		{
			$fitsAll = 1;
			print createBugInfoLine($bugData) . "\n";
		}
	}
	else
	{
		my $fits = 1;
		if(!fitsCondition($prio, $bugData->{$utilities::priorityHeader}))
		{
			$fits = 0;
		}
		if(!fitsCondition($diffi,$bugData->{$utilities::difficultyHeader}))
		{
			$fits = 0;
		}
		if(!fitsCondition($time, $bugData->{$utilities::timeHeader}))
		{
			$fits = 0;
		}
		if(!fitsCondition($ready, $bugData->{$utilities::readyHeader}))
		{
			$fits = 0;
		}
		if($fits)
		{
			$fitsAll = 1;
			
		}
		
	}
	if($fitsAll && !utilities::checkSolved($bugData))
	{
		print createBugInfoLine($bugData) . "\n";
	}
	
	
	return $line;
	
}




sub executeGetHint
{
	my ($line, $columnsData, $command) = @_;
	
	my $bugData = utilities::getBugLineData($csvParser, $columnsData, $line);
	
	if($bugData->{$utilities::bugIdHeader} eq $command->{$utilities::bugIdHeader})
	{
		if(!utilities::isTextVariableEmpty($bugData->{$utilities::wishgroupHeader}))
		{
			print "$bugData->{$utilities::wishgroupHeader}\n";
		}
	}
	return $line;
}


sub dateBreakdown
{
	my ($date) = @_;
	
	# 2016-01-24 15:19:19
	if($date =~ /(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/)
	{
		return ($1, $2, $3, $4, $5, $6);
	}
	else
	{
		return dateBreakdown($neverDate);
	}
	closeOnError("Date incorrect! date = " . $date);
}



sub dateStringLater
{
	my ($first, $second) = @_;
	
	
	my ($y1, $mon1, $d1, $h1, $min1, $s1) = dateBreakdown($first);
	my ($y2, $mon2, $d2, $h2, $min2, $s2) = dateBreakdown($second);
	
	return 1 if $y2 > $y1;
	return -1 if $y2 < $y1;
	
	return 1 if $mon2 > $mon1;
	return -1 if $mon2 < $mon1;
	
	return 1 if $d2 > $d1;
	return -1 if $d2 < $d1;
	
	return 1 if $h2 > $h1;
	return -1 if $h2 < $h1;
	
	return 1 if $min2 > $min1;
	return -1 if $min2 < $min1;
	
	
	return 1 if $s2 > $s1;
	return -1 if $s2 < $s1;
	
	return 0;
	
}



sub executeOutdated
{
	my ($line, $columnsData, $command) = @_;
	
	my $bugData = utilities::getBugLineData($csvParser, $columnsData, $line);
	
	my $changed = $bugData->{$utilities::changedHeader};
	my $lastRated = $bugData->{$utilities::lastRatedHeader};
	
	if(dateStringLater($changed, $lastRated) < 0 && utilities::isRated($bugData) && !utilities::checkSolved($bugData))
	{
		# it was rated before it was changed
		#print STDERR "dates: changed = $changed, last rated = $lastRated\n";
		print createBugInfoLine($bugData) . "\n";
	}
	
	return $line;
	
}

sub executeConfirm
{
	my ($line, $columnsData, $command) = @_;
	
	my $bugData = utilities::getBugLineData($csvParser, $columnsData, $line);
	
	
	if($bugData->{$utilities::bugIdHeader} eq $command->{$utilities::bugIdHeader})
	{
		if(!utilities::isRated($bugData))
		{
			closeOnError("The wish wasn't even rated yet!");
		}
		
		$bugData->{$utilities::lastRatedHeader} = getCurrentTimeField();
		
		return utilities::getBugLineFromData($csvParser, $columnsData, $bugData);
	}

	return $line;
	
}

sub executeGetToConfirm
{
	my ($line, $columnsData, $command) = @_;
	
	my $bugData = utilities::getBugLineData($csvParser, $columnsData, $line);
	
	
	if($bugData->{$utilities::bugIdHeader} eq $command->{$utilities::bugIdHeader})
	{
		if(!utilities::isRated($bugData))
		{
			closeOnError("The wish wasn't even rated yet!");
		}
		
		print "Rating for bug $bugData->{$utilities::bugIdHeader}: \npriority = $bugData->{$utilities::priorityHeader}\n"
		. "time = $bugData->{$utilities::timeHeader}\ndifficulty = $bugData->{$utilities::difficultyHeader}\nready = $bugData->{$utilities::readyHeader}\n"
		. "dates: changed = $bugData->{$utilities::changedHeader}, last rated = $bugData->{$utilities::lastRatedHeader}\n";
		
		return $line;
	}

	return $line;
	
}


sub createBugLink
{
	my ($bugNo) = @_;
	
	return "https://bugs.kde.org/show_bug.cgi?id=" . $bugNo;
}

sub createBugSpecialColumnsShort
{
	my ($bugData) = @_;
	
	my $prio = $bugData->{$utilities::priorityHeader};
	$prio = "." if utilities::isTextVariableEmpty($prio);
	$prio =~ s/high/H/g;
	$prio =~ s/medium/M/g;
	$prio =~ s/low/L/g;
	$prio =~ s/verylow/V/g;
	
	my $time = $bugData->{$utilities::timeHeader};
	$time = "." if utilities::isTextVariableEmpty($time);
	$time =~ s/short/S/g;
	$time =~ s/medium/M/g;
	$time =~ s/long/T/g;
	
	my $diffi = $bugData->{$utilities::difficultyHeader};
	$diffi = "." if utilities::isTextVariableEmpty($diffi);
	$diffi =~ s/difficult/D/g;
	$diffi =~ s/medium/M/g;
	$diffi =~ s/easy/E/g;
	
	
	my $ready = $bugData->{$utilities::readyHeader};
	$ready = "." if utilities::isTextVariableEmpty($ready);
	$ready =~ s/notready/N/g;
	$ready =~ s/ready/R/g;
	$ready =~ s/unsure/U/g;
	
	
	
	my $bugInfo = "$prio$diffi$time$ready";
	
	return $bugInfo;
}

sub createBugInfoLine
{
	my ($bugData) = @_;
	
	my $bugSpecialInfo = createBugSpecialColumnsShort($bugData);
	my $bugSpecialInfoLine = $bugSpecialInfo eq "...." ? "" : "$bugSpecialInfo\t";
	if(!defined $bugData->{$utilities::bugIdHeader})
	{
		print STDERR Dumper($bugData);
	}
	return $bugData->{$utilities::bugIdHeader} . "\t" . createBugLink($bugData->{$utilities::bugIdHeader}) . "\t$bugSpecialInfoLine" . $bugData->{$utilities::summaryHeader};
}





sub closeOnErrorUsage
{
	my ($reason) = @_;
	print STDERR $reason . "\n";
	print STDERR $usage;
	closeOnError($reason);
}



sub closeOnError 
{
	my ($reason) = @_;
	$reason = "" if not defined $reason;
	#copy($spreadsheetFilenameOriginal, $spreadsheetFilename);
	print STDERR "???\n";
	die "Error: " . $reason;
}




















