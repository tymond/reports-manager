#!/bin/bash


FILE=ImportantBugs.md

echo > $FILE

echo "## Bugs that are quite important to fix" >> $FILE
echo >> $FILE

echo "If you wish to fix one of the following bugs, please contact Krita team first!" >> $FILE
echo >> $FILE

echo "### Bugs: " >> $FILE
echo >> $FILE

TMP=tmp/tasks.tmp
echo -n > $TMP

./bugman.pl get high easy short ready | sed 's/$/(easy, short)/' >> $TMP
./bugman.pl get high easy medium ready | sed 's/$/(easy, medium)/' >> $TMP
./bugman.pl get high medium short ready | sed 's/$/(medium, short)/' >> $TMP

cat $TMP | uniq | scripts/printPrettyDivided.pl "bugs" >> $FILE



