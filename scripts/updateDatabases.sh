#!/bin/bash


# list of arguments expected in the input
optstring="hb:w:"

WISHES=""
WISHESDB=""

BUGS=""
BUGSDB=""

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      echo "$0 -b [bugs new database] -w [wishes new database]"
      echo "You don't need to update both databases"
      ;;
    w)
       WISHES=1
       WISHESDB=${OPTARG}
       ;;
    b)
       BUGS=1
       BUGSDB=${OPTARG}
       ;;
    :)
      echo "$0: Must supply an argument to -$OPTARG." >&2
      exit 1
      ;;
    ?)
      echo "Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done



if [ $WISHES ]
then
    echo "Update wishes!"
    scripts/updateDatabaseWishes.pl $WISHESDB
fi

if [ $BUGS ]
then
    echo "Update bugs!"
    scripts/updateDatabaseBugs.pl $BUGSDB
fi

