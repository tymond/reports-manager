#!/usr/bin/perl

package updateDatabaseReports;

use warnings;
use strict;
use File::Copy;

use Text::CSV;
use Data::Dumper;


use lib '.';
use utilities;



sub mergeColumns;
sub mergeBugs;

sub mergeBug;

sub saveBugs;



my $csvParser = Text::CSV->new({ sep_char => ',' });


# usage:
#./updateDatabase newDatabase.csv

sub executeEverything
{
	my ($newDatabase, $spreadsheetFilename, $spreadsheetFilenameOriginal, $tempFilename) = @_;
	


	copy($spreadsheetFilename, $spreadsheetFilenameOriginal);

	execute($newDatabase, $spreadsheetFilename, $tempFilename);

	copy($tempFilename, $spreadsheetFilename);

}


sub execute
{
	my ($newDatabase, $spreadsheetFilename, $tempFilename) = @_;
	# get data
	
	my ($oldBugs, $oldColumns) = getBugsFromSpreadsheet($csvParser, $spreadsheetFilename);
	my ($newBugs, $newColumns) = getBugsFromSpreadsheet($csvParser, $newDatabase);
	
	
	my $mergedColumns = mergeColumns($oldColumns, $newColumns);
	my $mergedBugs = mergeBugs($oldBugs, $newBugs);
	
	saveBugs($csvParser, $tempFilename, $mergedColumns, $mergedBugs);
	
}


sub getBugsFromSpreadsheet
{
	my ($csvParser, $filename) = @_;
	
	my $bugs = {};
	my $columnsData = {};
	
	open(my $in, "<:encoding(UTF-8)", $filename) or die "Cannot open file $filename for reading.";
	
	my $lineNo = 0;
	
	while(my $line = <$in>)
	{
		chomp $line;
		
		if($lineNo == 0)
		{
			$columnsData = utilities::createColumnsHeadersData($csvParser, $line);
			$lineNo += 1;
			next;
		}
		
		my $bug = utilities::getBugLineData($csvParser, $columnsData, $line);
		if(!defined $bug->{$utilities::bugIdHeader})
		{
			print STDERR "!!! Trying to find bug id header: $utilities::bugIdHeader in $bug";
			print STDERR Dumper($bug);
			print STDERR $line;
			utilities::closeOnError("!!!");
		}
		$bugs->{$bug->{$utilities::bugIdHeader}} = $bug;
		
		$lineNo += 1;
	}
	
	
	close($in);
	
	return ($bugs, $columnsData);
	
}

sub isSpecialColumn
{
	my ($column) = @_;
	
	return $column eq $utilities::priorityHeader
		|| $column eq $utilities::timeHeader
		|| $column eq $utilities::difficultyHeader
		|| $column eq $utilities::readyHeader
		|| $column eq $utilities::lastRatedHeader;
		
}

sub addSpecialColumns
{
	my ($columns) = @_;
	push @$columns, $utilities::priorityHeader;
	push @$columns, $utilities::timeHeader;
	push @$columns, $utilities::difficultyHeader;
	push @$columns, $utilities::readyHeader;
	push @$columns, $utilities::lastRatedHeader;
	return $columns;
}

sub mergeColumns
{
	my ($oldColumns, $newColumns) = @_;
	#print STDERR Dumper($oldColumns);
	#print STDERR Dumper($newColumns);
	
	my $columns = [sort { $oldColumns->{$a}{"id"} <=> $oldColumns->{$b}{"id"} } keys %$oldColumns]; # put all old columns here
	
	for my $key (sort { $newColumns->{$a}{"id"} <=> $newColumns->{$b}{"id"} } keys %$newColumns) # add the new ones
	{
		if(!defined $oldColumns->{$key}) 
		{
			push @$columns, $key; 
		}
	}
	
	#print STDERR "before sanitization: " . Dumper($columns);
	
	@$columns = grep { !isSpecialColumn($_) } @$columns;
	#print STDERR "after removal: " . Dumper($columns);
	
	$columns = addSpecialColumns($columns);
	#print STDERR "after sanitization: " . Dumper($columns);
	
	my $mergedColumns = {};
	
	for my $i (0..$#$columns)
	{
		#$mergedColumns->{$i} = $columns[$i];
		$mergedColumns->{$columns->[$i]} = {"id" => $i, "name" => $columns->[$i]};
	}
	
	
	return $mergedColumns;
}


sub mergeBugs
{
	my ($oldBugs, $newBugs) = @_;
	
	#print STDERR Dumper(keys %$oldBugs);
	#print STDERR Dumper(keys %$newBugs);
	
	my $mergedBugs = {};
	
	for my $key (keys %$oldBugs)
	{
		if(defined $newBugs->{$key})
		{
			my $bug = mergeBug($oldBugs->{$key}, $newBugs->{$key});
			$mergedBugs->{$key} = $bug;
		}
		else
		{
			$mergedBugs->{$key} = $oldBugs->{$key};
		}
	}
	
	my $all = 0;
	my $saved = 0;
	for my $key (keys %$newBugs)
	{
		$all += 1;
		if(!defined $oldBugs->{$key} && !utilities::checkSolved($newBugs->{$key}))
		{
			$saved += 1;
			$mergedBugs->{$key} = $newBugs->{$key};
		} elsif (!defined $oldBugs->{$key} && utilities::checkSolved($newBugs->{$key})) {
			#print STDERR "Dropping $key because it's solved.\n";
		}
	}
	
	return $mergedBugs;
	
}



sub mergeBug
{
	my ($oldBug, $newBug) = @_;
	my $mergedBug = {};
	
	
	for my $key (keys %$oldBug)
	{
		if(isSpecialColumn($key))
		{
			if(defined $newBug->{$key})
			{
				if($newBug->{$key} ne $oldBug->{$key})
				{
					print STDERR "Conflict for bug $oldBug->{$utilities::bugIdHeader} in column $key\n";
				}
				$mergedBug->{$key} = $newBug->{$key};
			}
			else
			{
				$mergedBug->{$key} = $oldBug->{$key};
			}
		}
		else
		{
			if(defined $newBug->{$key} and !($key eq "Votes" && $newBug->{$key} eq "0"))
			{
				$mergedBug->{$key} = $newBug->{$key};
			}
			else
			{
				$mergedBug->{$key} = $oldBug->{$key};
			}
		}
	}
	
	
	
	
	return $mergedBug;
}



sub saveBugs
{
	my ($csvParser, $tempFilename, $mergedColumns, $mergedBugs) = @_;
	
	my $filename = $tempFilename;
	open(my $out, ">:encoding(UTF-8)", $filename) or die "Cannot open file $filename for writing.";
	
	print $out utilities::getColumnsHeadersLine($csvParser, $mergedColumns) . "\n";
	
	
	for my $key (sort keys %$mergedBugs)
	{
		print $out utilities::getBugLineFromData($csvParser, $mergedColumns, $mergedBugs->{$key}) . "\n";
	}
	
	
}
