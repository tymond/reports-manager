#/bin/bash


echo "All rated:"
echo "----------"
scripts/countAllRatedWishes.sh
echo

echo "To rate:"
echo "--------"
echo -n "Wishes: "
./wishman.pl empty 1000 | wc -l
echo -n "Bugs: "
./bugman.pl empty 1000 | wc -l
echo

echo "Skipped:"
echo "--------"
echo -n "Wishes: "
if [ -f tmp/skippedWishes.txt ] ; then
    cat tmp/skippedWishes.txt | wc -l
else
    echo "0"
fi

echo -n "Bugs: "
if [ -f tmp/skippedBugs.txt ] ; then
    cat tmp/skippedBugs.txt | wc -l
else
    echo "0"
fi
echo
echo

echo "Changed:"
echo "--------"
scripts/countChangedWishes.sh


